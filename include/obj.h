#ifndef __OBJ_H__
#define __OBJ_H__

#include "glm/glm.hpp"
#include "glm/ext.hpp"

#include <string>
#include <vector>



class Obj
{
	public:
		Obj ();
		Obj (std::string filename);
		
		bool load (std::string filename = std::string());
		std::vector<glm::vec3> const& getPositions() const;
		std::vector<glm::vec3> const& getNormals() const;
		std::vector<glm::vec2> const& getTexCoords() const;
		
		bool isLoaded () const;
		
	private:
		std::string objName;
		std::vector<glm::vec3> positions;
		std::vector<glm::vec3> normals;
		std::vector<glm::vec2> texCoords;
		
		bool loaded_;
};

#endif /* __OBJ_H__ */

