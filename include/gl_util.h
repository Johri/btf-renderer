#ifndef __GL_UTIL_FUNCTIONS_H__
#define __GL_UTIL_FUNCTIONS_H__

#include "gl_core_4_2.h"

#include <GL/glu.h>

#include <string>
#include <iostream>

inline
bool printProgramInfoLog(GLuint program)
{
	int infologLength = 0;
	std::string infoLog;
	
	GLint result = GL_FALSE;
	glValidateProgram(program);
	glGetProgramiv(program, GL_VALIDATE_STATUS, &result);
	
	glGetProgramiv(program, GL_INFO_LOG_LENGTH, &infologLength);

	if (infologLength > 1)
	{
		infoLog.resize(infologLength);
		glGetProgramInfoLog(program, infologLength, nullptr, &infoLog[0]);
		std::cout << infoLog << std::endl;
	}
	
	return result == 1;
}

inline
bool printShaderInfoLog(GLuint shader)
{
	int infologLength = 0;
	std::string infoLog;
	
	GLint result = GL_FALSE;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &result);
	
	glGetShaderiv(shader, GL_INFO_LOG_LENGTH,&infologLength);

	if (infologLength > 1)
	{
		infoLog.resize(infologLength);
		glGetShaderInfoLog(shader, infologLength, nullptr, &infoLog[0]);
		std::cout << infoLog << std::endl;
	}
	
	return result == 1;
}

inline
void debugOutput (GLenum source, 
                             GLenum type, 
                             GLuint id, 
                             GLenum severity, 
                             GLsizei length, 
                             const GLchar* message, 
                             GLvoid* userParam)
{
	if (id == 131204 || id == 131185)
	{
		return;
	}
	std::string debSource;
	std::string debType;
	std::string debSev;

	if(source == GL_DEBUG_SOURCE_API_ARB)
		debSource = "OpenGL";
	else if(source == GL_DEBUG_SOURCE_WINDOW_SYSTEM_ARB)
		debSource = "Window";
	else if(source == GL_DEBUG_SOURCE_SHADER_COMPILER_ARB)
		debSource = "Shader Compiler";
	else if(source == GL_DEBUG_SOURCE_THIRD_PARTY_ARB)
		debSource = "Third Party";
	else if(source == GL_DEBUG_SOURCE_APPLICATION_ARB)
		debSource = "Application";
	else if(source == GL_DEBUG_SOURCE_OTHER_ARB)
		debSource = "Other";
 
	if(type == GL_DEBUG_TYPE_ERROR_ARB)
		debType = "error";
	else if(type == GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR_ARB)
		debType = "deprecated behavior";
	else if(type == GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR_ARB)
		debType = "undefined behavior";
	else if(type == GL_DEBUG_TYPE_PORTABILITY_ARB)
		debType = "portability";
	else if(type == GL_DEBUG_TYPE_PERFORMANCE_ARB)
		debType = "performance";
	else if(type == GL_DEBUG_TYPE_OTHER_ARB)
		debType = "message";
//	Core in OpenGL 4.3
//	else if(type == GL_DEBUG_TYPE_MARKER)
//		debType = "portability";
//	else if(type == GL_DEBUG_TYPE_PUSH_GROUP)
//		debType = "portability";
//	else if(type == GL_DEBUG_TYPE_POP_GROUP)
//		debType = "performance";
 
	if(severity == GL_DEBUG_SEVERITY_HIGH_ARB)
		debSev = "high";
	else if(severity == GL_DEBUG_SEVERITY_MEDIUM_ARB)
		debSev = "medium";
	else if(severity == GL_DEBUG_SEVERITY_LOW_ARB)
		debSev = "low";
//	Core in OpenGL 4.3
//	else if(severity == GL_DEBUG_SEVERITY_NOTIFICATION)
//		debSev = "notification";

	std::cerr << debSource << ": " << debType << " (" << debSev << ") " << id << ": " << message << std::endl;
	
//	if (severity == GL_DEBUG_SEVERITY_HIGH_ARB) 
//	{
//		throw std::runtime_error(std::string("OpenGL Error"));
//	}
}

inline
void check_gl_errors(const char* msg, bool th = true)
{
	bool error = false;
	GLuint errnum;
	const char *errstr;
	while ((errnum = glGetError())) 
	{
		errstr = reinterpret_cast<const char *>(gluErrorString(errnum));
		
		std::cout << "GLError: " << errstr << std::endl;
		std::cout << "	 at: " << msg << std::endl;
		std::cout.flush();
		error = true;
	}
	
	if (th && error) 
	{
//		throw std::runtime_error(std::string("OpenGL Error at ") + std::string(msg));
	}

}


#endif /* __GL_UTIL_FUNCTIONS_H__ */

