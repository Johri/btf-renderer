#ifndef __BTF_UTILITY_H__
#define __BTF_UTILITY_H__

#include "gl_core_4_2.h"

#include <jpeglib.h>

#include <vector>
#include <iostream>
#include <fstream>



inline
std::vector<char> readRawImage (std::string filename)
{
	std::vector<char> data;

	if (filename.substr(filename.size()-3) != "raw")
	{
		std::cerr << "\nInvalid image type selected, file is not of type \"raw\"" << std::endl;
	}
	else
	{	
		std::ifstream file (filename, std::ifstream::binary);
		unsigned int fileLength = 0;
		if (file)
		{
			file.seekg(0, std::ifstream::end);
			fileLength = unsigned(file.tellg());
			file.seekg(0, std::ifstream::beg);
		
			data.resize(fileLength);
		
			file.read (data.data(), fileLength);
			file.close ();
		}
		else
		{
			std::cerr << "\nCould not read file: " << filename << std::endl;
		}
	}
	return data;	
}


inline
std::vector<char> readBmpImage (std::string filename)
{
	std::vector<char> data;
	if (filename.substr(filename.size()-3) != "bmp")
	{
		std::cerr << "\nInvalid image type selected, file is not of type \"bmp\"" << std::endl;
	}
	else
	{	
		std::ifstream file (filename, std::ifstream::binary);
		unsigned int fileLength = 0;
		if (file)
		{
			file.seekg(0, std::ifstream::end);
			fileLength = unsigned(file.tellg()) - 54;
			file.seekg(54, std::ifstream::beg);
		
			data.resize(fileLength);
		
			file.read (data.data(), fileLength);
			file.close ();
		}
		else
		{
			std::cerr << "\nCould not read file: " << filename << std::endl;
		}
	}
	return data;	
}


inline
std::vector<char> readJpegImage (std::string filename)
{
	std::vector<char> data;
	
	std::string ext = filename.substr(filename.size()-3);
	if (ext != "jpg" && ext != "JPG" && ext != "jpeg")
	{
		std::cerr << "\nInvalid image type selected, file is not of type \"jpg\"" << std::endl;
	}
	else
	{
		/* these are standard libjpeg structures for reading(decompression) */
		struct jpeg_decompress_struct cinfo;
		struct jpeg_error_mgr jerr;
		/* libjpeg data structure for storing one row, that is, scanline of an image */
		JSAMPROW row_pointer[1];
	
		FILE *infile = fopen( filename.c_str(), "rb" );
		unsigned long location = 0;
	
		if ( !infile )
		{
			std::cerr << "\nCould not read file: " << filename << std::endl;
			return data;
		}
		/* here we set up the standard libjpeg error handler */
		cinfo.err = jpeg_std_error( &jerr );
		/* setup decompression process and source, then read JPEG header */
		jpeg_create_decompress( &cinfo );
		/* this makes the library read from infile */
		jpeg_stdio_src( &cinfo, infile );
		/* reading the image header which contains image information */
		jpeg_read_header( &cinfo, TRUE );
		/* Uncomment the following to output image information, if needed. */
	
//		printf( "JPEG File Information: \n" );
//		printf( "Image width and height: %d pixels and %d pixels.\n", cinfo.image_width, cinfo.image_height );
//		printf( "Color components per pixel: %d.\n", cinfo.num_components );
//		printf( "Color space: %d.\n", cinfo.jpeg_color_space );
	
		/* Start decompression jpeg here */
		jpeg_start_decompress(&cinfo);

		/* allocate memory to hold the uncompressed image */
		data.resize(cinfo.output_width * cinfo.output_height * cinfo.num_components);
		/* now actually read the jpeg into the raw buffer */
		row_pointer[0] = (unsigned char *)malloc( cinfo.output_width*cinfo.num_components);
		/* read one scan line at a time */
		while (cinfo.output_scanline < cinfo.image_height)
		{
			jpeg_read_scanlines( &cinfo, row_pointer, 1 );
			for (unsigned int i = 0; i < cinfo.image_width * cinfo.num_components; i++)
			{
				data[location++] = row_pointer[0][i];
			}
		}
		/* wrap up decompression, destroy objects, free pointers and close open files */
		jpeg_finish_decompress( &cinfo );
		jpeg_destroy_decompress( &cinfo );
		free( row_pointer[0] );
		fclose( infile );
		/* yup, we succeeded! */
	}
	return data;
}


inline
bool writeCurrentFrameToBMP(std::string filename, unsigned int x, unsigned int y, unsigned int width, unsigned int height)
{
	glBindFramebuffer(GL_READ_FRAMEBUFFER, 0);
	glReadBuffer(GL_BACK);
	glPixelStorei(GL_PACK_ALIGNMENT, 1);

	std::vector<unsigned char> data;
	data.resize(width * height * 3);
	
	glReadPixels(x, y, width, height, GL_BGR, GL_UNSIGNED_BYTE, data.data());
	
	std::ofstream file (filename, std::ofstream::binary);
	if(!file)
	{
		return false;
	}
	
	char c;
	unsigned short s;
	int i;
	unsigned int u;
	
	c = 'B'; file.write(&c, 1); c = 'M'; file.write(&c, 1);
	i = width * height * 3 + 54; file.write((char const*) &i, 4);
	i = 0; file.write((char const*) &i,4);
	i = 54; file.write((char const*) &i, 4);
	
	i = 40; file.write((char const*) &i, 4);
	u = width;  file.write((char const*) &u, 4);
	u = height; file.write((char const*) &u, 4);
	s = 1; file.write((char const*) &s, 2);
	s = 24; file.write((char const*) &s, 2);
	i = 0; file.write((char const*) &i, 4);
	i = width * height * 3; file.write((char const*) &i, 4);
	u = 0; file.write((char const*) &u, 4);
	u = 0; file.write((char const*) &u, 4);
	i = 0; file.write((char const*) &i, 4);
	i = 0; file.write((char const*) &i, 4);
	
	file.write((char const*) &data[0], width*height*3);

	file.close();
	
	return true;
}


#endif /* __BTF_UTILITY_H__ */

