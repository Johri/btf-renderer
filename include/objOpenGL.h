#ifndef __OBJOPENGL_H__
#define __OBJOPENGL_H__

#include "obj.h"
#include "gl_core_4_2.h"

#include <vector>
#include <string>
#include <iostream>


class ObjOpenGL
{
	public:
		bool loadObj (std::string filename, bool tangent);
		void draw ();
		
	private:
		void calcTangent(std::vector<glm::vec3> const& pos, std::vector<glm::vec2> const& texCoord, std::vector<glm::vec3>& tangent, std::vector<glm::vec3>& bitangent);
		
		Obj object_;
		GLuint vaoID_;
};

#endif /* __OBJOPENGL_H__ */

