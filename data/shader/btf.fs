#version 420
//
uniform int textureOffset;
uniform sampler2DArray texArray[24];
//uniform sampler2DArray texArray1[4];   // Martin Johr //
uniform int texArraySize;
//uniform int texArraySize2;  // Martin Johr //

//uniform vec2 focus;

// Martin Johr //
uniform vec2 POI;
uniform int Radius_px;
uniform int radius_two;
uniform int foveated;
uniform int interval_switch;
uniform int smoothswitch = 3; // 0 = immediately switch between Qualitys; 1 = random; 2 = linear interpolated random; 3 = linear interpolated
//uniform int texCounter;
//uniform sampler2D tex;
//
uniform float phiInterval_256[6] = {360.0, 60.0, 30.0, 20.0, 18.0, 15.0};
uniform float phiInterval_400[10] = {360.0, 60.0, 30.0, 30.0, 30.0, 20.0, 20.0, 15.0, 15.0, 15.0};
uniform float phiOffsets_400[10] = {0.0, 15.0, 0.0, 15.0, 0.0, 0.0, 7.5, 0.0, 7.5, 0.0};
uniform float phiIntervalCount_256[7] = {0.0, 1.0, 7.0, 19.0, 37.0, 57.0, 81.0};
uniform float phiIntervalCount_400[11] = {0.0, 1.0, 7.0, 19.0, 31.0, 43.0, 61.0, 79.0, 103.0, 127.0, 151.0};
uint texPerViewDir_256 = 81;
uint texPerViewDir_400 = 151;
uniform float thetaInterval_256 = 15.0;
uniform float thetaIntervals_256[6] = {15.0, 15.0, 15.0, 15.0, 15.0, 15.0};
uniform float thetaOffsets_256[6] = {0.0, 15.0, 30.0, 45.0, 60.0, 75.0};
uniform float thetaOffsets_400[10] = {0.0, 11.0, 23.5, 30.0, 37.5, 45.0, 52.5, 60.0, 67.5, 75.0};
uniform float thetaIntervalCount_400[11] = {0.0, 1.0, 7.0, 19.0, 31.0, 43.0, 61.0, 79.0, 103.0, 127.0, 151.0};
uniform float thetaMaxAngle = 75.0;
uniform vec3 posLightEye = vec3 (75.0, 0.0, 0.0);
uniform float thetaLight1 = 1.0;
uniform float phiLight1 = 1.0;
uniform float thetaLight2 = 1.0;
uniform float phiLight2 = 1.0;
uniform float thetaView1 = 1.0;
uniform float phiView1 = 1.0;
uniform float thetaView2 = 1.0;
uniform float phiView2 = 1.0;
uniform float tcm1 = 7.0;
uniform float tcm2 = 7.0;
uniform int objectID = 0;
//
in vec4 vPosition;
in vec2 vTexCoord;
in vec3 vNormalEye;
in vec3 vTangentEye;
in vec3 vBiTangentEye;
//in mat3 tbn;
//
out vec4 fragColor;
//
const vec3 posViewEye = vec3 (0.0, 0.0, 0.0);

float thetaLightEach = 0.0;
float phiLightEach = 0.0;
float thetaViewEach = 0.0;
float phiViewEach = 0.0;
float tcm = 0.0;

vec4 mixPhiLight(in int thetaLightID, in float phiLight, in float texIDAccum, in int TexID)
{
    vec4 phiLightFlCol;
	vec4 phiLightCeCol;

	float phiLightID;
	float phiLightFl;
	float phiLightCe;
	float phiLightMix;
	
	float texIDAccumFl;
	float texIDAccumCe;

    if(interval_switch == 0.0)
    {
        phiLightFlCol = vec4(0, 0, 0, 0);
	    phiLightCeCol = vec4(0, 0, 0, 0);

	    phiLightID = phiLight / phiInterval_256[thetaLightID] / phiLightEach;
	    phiLightFl  = floor(phiLightID) * phiLightEach;
	    phiLightCe  = ceil (phiLightID) * phiLightEach;
	    phiLightMix = fract(phiLightID);
	
	    texIDAccumFl = texIDAccum + phiIntervalCount_256[thetaLightID] + phiLightFl;
	    texIDAccumCe = texIDAccum + phiIntervalCount_256[thetaLightID] + phiLightCe;
	
    //	if (phiIntervalCount_256[thetaLightID] + phiLightCe >= phiIntervalCount_256[thetaLightID+1])
	    if (phiLight + phiInterval_256[int(thetaLightID)] >= 360)
	    {
		    texIDAccumCe = texIDAccum + phiIntervalCount_256[thetaLightID];
	    }
    }
    else if(interval_switch == 1.0)
    {
        phiLightFlCol = vec4(0, 0, 0, 0);
	    phiLightCeCol = vec4(0, 0, 0, 0);

	    phiLightID = phiLight / phiInterval_400[thetaLightID] / phiLightEach;
	    phiLightFl  = floor(phiLightID) * phiLightEach;
	    phiLightCe  = ceil (phiLightID) * phiLightEach;
	    phiLightMix = fract(phiLightID);
	
	    texIDAccumFl = texIDAccum + phiIntervalCount_400[thetaLightID] + phiLightFl;
	    texIDAccumCe = texIDAccum + phiIntervalCount_400[thetaLightID] + phiLightCe;
	
        //	if (phiIntervalCount_400[thetaLightID] + phiLightCe >= phiIntervalCount_400[thetaLightID+1])
	    if (phiLight + phiInterval_400[int(thetaLightID)] >= 360)
	    {
		    texIDAccumCe = texIDAccum + phiIntervalCount_400[thetaLightID];
	    }
    }


#ifdef __GLSL_CG_DATA_TYPES

    // Martin Johr //
	if ( TexID == 0 ) // INSIDE
    {
        phiLightFlCol = texture(texArray[int(floor((texIDAccumFl) / 2048) + textureOffset*TexID)], vec3(vTexCoord * tcm, mod(texIDAccumFl, 2048))); //  "+ (6561 * TexID)" by Martin Johr
	    phiLightCeCol = texture(texArray[int(floor((texIDAccumFl) / 2048) + textureOffset*TexID)], vec3(vTexCoord * tcm, mod(texIDAccumCe, 2048))); //  "+ (6561 * TexID)" is INSIDE not necessary, because TexID is 0 anyway.
    }
    else // if ( TexID == 1 ) // OUTSIDE
    {
        phiLightFlCol = texture(texArray[int(floor((texIDAccumFl) / 2048) + textureOffset*TexID)], vec3(vTexCoord * tcm, mod(texIDAccumFl, 2048))); //  "+ (6561 * TexID)" to address the right offset but offset could be wrong ...
	    phiLightCeCol = texture(texArray[int(floor((texIDAccumFl) / 2048) + textureOffset*TexID)], vec3(vTexCoord * tcm, mod(texIDAccumCe, 2048))); //  "+ (6561 * TexID)" ... or already discribed gab (see main.cpp Z319) could cause issues.
    }
#else // this part does nothing on my graficscard
	vec4 fl[10];
	vec4 ce[10];

	//if ( TexID == 0 )
    //{
        for (uint i = 0; i < texArraySize1; ++i)
	    {
		        fl[i] = texture(texArray[i], vec3(vTexCoord * tcm, mod(texIDAccumFl, 2048)));
		        ce[i] = texture(texArray[i], vec3(vTexCoord * tcm, mod(texIDAccumCe, 2048)));
	    }
    //}
    //else // if ( TexID == 1 ) 
    //{
    //    for (uint i = 0; i < texArraySize2; ++i)
	//    {
	//	        fl[i] = texture(texArray[i], vec3(vTexCoord * tcm, mod(texIDAccumFl,2048)));
	//	        ce[i] = texture(texArray[i], vec3(vTexCoord * tcm, mod(texIDAccumCe,2048)));
    //    }
	//}
    
	phiLightFlCol = fl[int(floor(texIDAccumFl/2048))];
	phiLightCeCol = ce[int(floor(texIDAccumCe/2048))];
#endif

	return mix(phiLightFlCol, phiLightCeCol, phiLightMix);
}

vec4 mixThetaLight(in float thetaLight, in float phiLight, in float texIDAccum, in int TexID)
{
        float thetaLightID;
	    float thetaLightFl;
	    float thetaLightCe;
	    float thetaLightMix;

	    vec4 thetaLightFlCol;
	    vec4 thetaLightCeCol;
        
	    float maxThetaLightID;

    if(interval_switch == 0.0)
    {
        // nearest lower angle, nearest higher angle and interpolation factor
        for(int i = 0; i < 6; i++)
        {
            if(thetaLight > thetaOffsets_256[i])
            {
                if(i < 5)
                {
	                thetaLightID = (i + ( (thetaLight - thetaOffsets_256[i]) / (thetaOffsets_256[i+1] - thetaOffsets_256[i]) )) / thetaLightEach;
                }
                else
                {
	                thetaLightID = (i + ( (thetaLight - thetaOffsets_256[i]) / (thetaOffsets_256[i] - thetaOffsets_256[i-1]) )) / thetaLightEach;
                }
            }
        }
	    thetaLightFl  = floor(thetaLightID) * thetaLightEach;
	    thetaLightCe  = ceil (thetaLightID) * thetaLightEach;
	    thetaLightMix = fract(thetaLightID);

	    thetaLightFlCol = mixPhiLight(int(thetaLightFl), phiLight, texIDAccum, TexID);
	    thetaLightCeCol = mixPhiLight(int(thetaLightCe), phiLight, texIDAccum, TexID);
	
	    // upper bound, if higher -> no light/shadow
	    // e.g. theta = 80
	    // =>   thetaFloor = 75 -- ok
	    //      thetaCeil  = 90 !! not available, no light
	    //  ==> thetaCeilColor  = black
	    maxThetaLightID = 5;
    }
    else if(interval_switch == 1.0)
    {
	    // nearest lower angle, nearest higher angle and interpolation factor
        for(int i = 0; i < 10; i++)
        {
            if(thetaLight > thetaOffsets_400[i])
            {
                if(i < 9)
                {
	                thetaLightID = (i + ( (thetaLight - thetaOffsets_400[i]) / (thetaOffsets_400[i+1] - thetaOffsets_400[i]) )) / thetaLightEach;
                }
                else
                {
	                thetaLightID = (i + ( (thetaLight - thetaOffsets_400[i]) / (thetaOffsets_400[i] - thetaOffsets_400[i-1]) )) / thetaLightEach;
                }
            }
        }
	    thetaLightFl  = floor(thetaLightID) * thetaLightEach;
	    thetaLightCe  = ceil (thetaLightID) * thetaLightEach;
	    thetaLightMix = fract(thetaLightID);

	    thetaLightFlCol = mixPhiLight(int(thetaLightFl), phiLight, texIDAccum, TexID);
	    thetaLightCeCol = mixPhiLight(int(thetaLightCe), phiLight, texIDAccum, TexID);
	
	    // upper bound, if higher -> no light/shadow
	    // e.g. theta = 80
	    // =>   thetaFloor = 75 -- ok
	    //      thetaCeil  = 90 !! not available, no light
	    //  ==> thetaCeilColor  = black
	    maxThetaLightID = 9;
    }

	if (thetaLightFl > maxThetaLightID)
	{
		thetaLightFlCol = vec4(0.0, 0.0, 0.0, 1.0);
	}
	if (thetaLightCe > maxThetaLightID)
	{
		thetaLightCeCol = vec4(0.0, 0.0, 0.0, 1.0);
	}	

	
	return mix(thetaLightFlCol, thetaLightCeCol, thetaLightMix);
}


vec4 mixPhiView(in int thetaViewID, in float phiView, in float thetaLight, in float phiLight, in int TexID)
{
    float phiViewID;
	float phiViewFl;
	float phiViewCe;
	float phiViewMix;
	
	float texIDAccumFl;
	float texIDAccumCe;

    if(interval_switch == 0.0)
    {
	    phiViewID = phiView / phiInterval_256[thetaViewID] / phiViewEach;
	    phiViewFl  = floor(phiViewID) * phiViewEach;
	    phiViewCe  = ceil (phiViewID) * phiViewEach;
	    phiViewMix = fract(phiViewID);
	
	    texIDAccumFl = texPerViewDir_256 * (phiIntervalCount_256[thetaViewID]+phiViewFl);
	    texIDAccumCe = texPerViewDir_256 * (phiIntervalCount_256[thetaViewID]+phiViewCe);
	
	    // upper bound, wrap around to phi = 0
	    // e.g. phi = 345, interval = 60
	    // =>   phiFloor = 300
	    //      phiCeil  = 360 !! 360 not available, but 360 == 0
	    //  ==> phiCeil  = 0
    //	if (phiIntervalCount_256[thetaViewID] + phiViewCe >= phiIntervalCount_256[thetaViewID+1])
	    if (phiView + phiInterval_256[int(thetaViewID)] >= 360)
	    {
		    texIDAccumCe = texPerViewDir_256 * phiIntervalCount_256[thetaViewID];
	    }
	}
    else if(interval_switch == 1.0)
    {
	    phiViewID = phiView / phiInterval_400[thetaViewID] / phiViewEach;
	    phiViewFl  = floor(phiViewID) * phiViewEach;
	    phiViewCe  = ceil (phiViewID) * phiViewEach;
	    phiViewMix = fract(phiViewID);
	
	    texIDAccumFl = texPerViewDir_400 * (phiIntervalCount_400[thetaViewID]+phiViewFl);
	    texIDAccumCe = texPerViewDir_400 * (phiIntervalCount_400[thetaViewID]+phiViewCe);
	
	    // upper bound, wrap around to phi = 0
	    // e.g. phi = 345, interval = 60
	    // =>   phiFloor = 300
	    //      phiCeil  = 360 !! 360 not available, but 360 == 0
	    //  ==> phiCeil  = 0
    //	if (phiIntervalCount_400[thetaViewID] + phiViewCe >= phiIntervalCount_400[thetaViewID+1])
	    if (phiView + phiInterval_400[int(thetaViewID)] >= 360)
	    {
		    texIDAccumCe = texPerViewDir_400 * phiIntervalCount_400[thetaViewID];
	    }
	}

	vec4 phiViewFlCol = mixThetaLight(thetaLight, phiLight, texIDAccumFl, TexID);
	vec4 phiViewCeCol = mixThetaLight(thetaLight, phiLight, texIDAccumCe, TexID);
	
	return mix(phiViewFlCol, phiViewCeCol, phiViewMix);
}


vec4 calcColor(in float thetaView, in float phiView, in float thetaLight, in float phiLight, in int TexID, in float validation)
{
    if(validation == 0)
    {
        return vec4(0.0, 0.0, 0.0, 0.0);
    }
    
	float thetaViewID;
	float thetaViewFl;
	float thetaViewCe;
	float thetaViewMix;
	
	float maxThetaViewID;

    if(interval_switch == 0.0)
    {
	    // nearest lower angle, nearest higher angle and interpolation factor
        for(int i = 0; i < 6; i++)
        {
            if(thetaView > thetaOffsets_256[i])
            {
                if(i < 5)
                {
	                thetaViewID = (i + ( (thetaView - thetaOffsets_256[i]) / (thetaOffsets_256[i+1] - thetaOffsets_256[i]) )) / thetaViewEach;
                }
                else
                {
	                thetaViewID = (i + ( (thetaView - thetaOffsets_256[i]) / (thetaOffsets_256[i] - thetaOffsets_256[i-1]) )) / thetaViewEach;
                }
            }
        }
	    thetaViewFl  = floor(thetaViewID) * thetaViewEach;
	    thetaViewCe  = ceil (thetaViewID) * thetaViewEach;
	    thetaViewMix = fract(thetaViewID);
	
	    // upper bound, clamp to highest available theta
	    // e.g. theta = 80
	    // =>   thetaFloor = 75
	    //      thetaCeil  = 90 !! not available
	    //  ==> thetaCeil  = 75
	    maxThetaViewID = 5;
    }
    else if(interval_switch == 1.0)
    {
	    // nearest lower angle, nearest higher angle and interpolation factor
        for(int i = 0; i < 10; i++)
        {
            if(thetaView > thetaOffsets_400[i])
            {
                if(i < 9)
                {
	                thetaViewID = (i + ( (thetaView - thetaOffsets_400[i]) / (thetaOffsets_400[i+1] - thetaOffsets_400[i]) )) / thetaViewEach;
                }
                else
                {
	                thetaViewID = (i + ( (thetaView - thetaOffsets_400[i]) / (thetaOffsets_400[i] - thetaOffsets_400[i-1]) )) / thetaViewEach;
                }
            }
        }
	    thetaViewFl  = floor(thetaViewID) * thetaViewEach;
	    thetaViewCe  = ceil (thetaViewID) * thetaViewEach;
	    thetaViewMix = fract(thetaViewID);
	
	    // upper bound, clamp to highest available theta
	    // e.g. theta = 80
	    // =>   thetaFloor = 75
	    //      thetaCeil  = 90 !! not available
	    //  ==> thetaCeil  = 75
	    maxThetaViewID = 9;
    }

	if (thetaViewFl > maxThetaViewID)
	{
		thetaViewFl = maxThetaViewID;
//		thetaViewCeCol = vec4(0.0, 0.0, 0.0, 1.0);
	}
	if (thetaViewCe > maxThetaViewID)
	{
		thetaViewCe = maxThetaViewID;
//		thetaViewCeCol = vec4(0.0, 0.0, 0.0, 1.0);
	}
	
	vec4 thetaViewFlCol = mixPhiView(int(thetaViewFl), phiView, thetaLight, phiLight, TexID);
	vec4 thetaViewCeCol = mixPhiView(int(thetaViewCe), phiView, thetaLight, phiLight, TexID);
	
	return mix(thetaViewFlCol, thetaViewCeCol, thetaViewMix);
}




float rand(vec2 co)
{
    return fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453);
}





// funktions to replace if clauses // http://theorangeduck.com/page/avoiding-shader-conditionals

float when_equal(float x, float y) {
  return 1.0 - abs(sign(x - y));
}

float when_not_equal(float x, float y) {
  return abs(sign(x - y));
}

float when_greater_then(float x, float y) {
  return max(sign(x - y), 0.0);
}

float when_lower_then(float x, float y) {
  return max(sign(y - x), 0.0);
}

float when_greater_equal(float x, float y) {
  return 1.0 - when_lower_then(x, y);
}

float when_lower_equal(float x, float y) {
  return 1.0 - when_greater_then(x, y);
}






void main()
{
    // transformation to tangent space
	mat3 tbn = inverse(mat3(normalize(vTangentEye), normalize(vBiTangentEye), normalize(vNormalEye)));
	
	vec3 dirLightTangent = tbn * (posLightEye - vPosition.xyz);
	vec3 dirViewTangent = tbn * (posViewEye - vPosition.xyz);
	vec3 normalTangent = normalize(tbn * vNormalEye);
	vec3 tangentTangent = normalize(tbn * vTangentEye);
	vec3 biTangentTangent = normalize(tbn * vBiTangentEye);
		

	// calculate different angles
	float tld = dot(normalTangent.xyz, normalize(dirLightTangent));
	float thetaLight = degrees(acos(tld));
	
	vec3 dltt = vec3(dirLightTangent.xy, 0.0);
	float pltd = dot(tangentTangent, normalize(dltt));
	float phiLight = 360.0 - degrees(acos(pltd));   // float phiLight = degrees(acos(pltd));
	if (dirLightTangent.y < 0.0)
	{
		phiLight = 360.0 - phiLight;
	}
	
	float tvd = dot(normalTangent.xyz, normalize(dirViewTangent));
	float thetaView = degrees(acos(tvd));
	
	vec3 dvtt = vec3(dirViewTangent.xy, 0.0);
	float pvtd = dot(tangentTangent, normalize(dvtt));
	float phiView = 360.0 - degrees(acos(pvtd));    // float phiView = degrees(acos(pvtd));
	if (dirViewTangent.y < 0.0)
	{
		phiView = 360.0 - phiView;
	}




    vec2 FC = vec2(gl_FragCoord.x, gl_FragCoord.y);
    float rando = rand(FC);

    float pow_distance = pow( gl_FragCoord.x - POI.x , 2 ) + pow( gl_FragCoord.y - POI.y , 2 );

    int what_to_render = 0; // 0 = REDDOT; 1 = FIRST QUALITY (probably inside); 2 = SECOND QUALITY (probably outside); 3 = INTERPOLATED QUALITY (probably inbetween) -> affect what and how to render
    int texture_code = 0;
    int both_rad = Radius_px + radius_two;

    vec3 col = vec3(1.0, 0.0, 1.0); // Purple for nothing. Nothing should be purple.
    

    // Martin Johr //
    if (foveated == 1.0)
    {

        if ( pow_distance <= pow( 2, 2 ) ) // REDDOT
        {
            what_to_render = 0;
        }
        else if ( pow_distance <= pow( Radius_px, 2 ) ) // INSIDE
        {
            //fragColor = vec4(vNormalEye.x, vNormalEye.y, vNormalEye.z, 1.0); // Normalenvisualisierung
        
            // interpolate final color
            // ODER nur einzeln berechnen !!!!!!!!!!
            
	        what_to_render = 1;
        }
        else
        {
            if ( pow_distance <= pow( both_rad, 2 ) ) // INBETWEEN
            {

                if (smoothswitch == 0)
                {
                    what_to_render = 2;
                }
                else if (smoothswitch == 1)
                {
                    if(rando <= 0.5)
                    {
                        what_to_render = 1;
                        //fragColor = vec4( 1.0, 0.0, 0.0, 1.0);
                    }
                    else
                    {
                        what_to_render = 2;
                        //fragColor = vec4( 0.0, 0.0, 1.0, 1.0);
                    }
                }
                else if (smoothswitch == 2)
                {
                    float a = both_rad - sqrt(pow_distance); // a is the difference between radius_two (fading-ring) and the distance from the mouse to the Fragment (from Fragment to outer ring)
                    
                    if ( a / radius_two >= rando)
                    {
                        what_to_render = 1;
                        //fragColor = vec4( 1.0, 0.0, 0.0, 1.0);
                    }
                    else
                    {
                        what_to_render = 2;
                        //fragColor = vec4( 0.0, 0.0, 1.0, 1.0);
                    }
                }
                else if (smoothswitch == 3)
                {
                    what_to_render = 3;
                }
            }
            else    // OUTSIDE
            {
	            // interpolate final color
                // ODER nur einzeln berechnen !!!!!!!!!!
            
	            what_to_render = 2;
            }
        }
    }
    else
    {
        if( pow_distance <= 4 ) // REDDOT // thetaView > 59.9 && thetaView < 60.1 || thetaView < 0.5 || phiView > 71.9 && phiView < 72.1 || thetaLight > 32.9 && thetaLight < 33.1 || phiLight > 71.460 && phiLight < 72.465 ) // dirLightTangent.y < 0.01 && dirLightTangent.y > -0.01 || dirLightTangent.x < 0.01 && dirLightTangent.x > -0.01 ) 
        {
            what_to_render = 0;
        }
        else
        {
	        what_to_render = 1;
        }
    }

	
    
    vec3 col0;
    vec3 col1;

	tcm = tcm1;

    //if ( what_to_render == 1 ) // render first Quality
    //{
		thetaLightEach = thetaLight1;
		phiLightEach = phiLight1;
		thetaViewEach = thetaView1;
		phiViewEach = phiView1;
        
        col0 = calcColor(thetaView, phiView, thetaLight, phiLight, 0, when_equal(what_to_render, 1) + when_equal(what_to_render, 3)).xyz * (when_equal(what_to_render, 1) + when_equal(what_to_render, 3) );

    //}
	//else if ( what_to_render == 2 ) // render second Quality
	//{
		thetaLightEach = thetaLight2;
		phiLightEach = phiLight2;
		thetaViewEach = thetaView2;
		phiViewEach = phiView2;
        
        col1 = calcColor(thetaView, phiView, thetaLight, phiLight, 1, when_equal(what_to_render, 2) + when_equal(what_to_render, 3)).xyz * ( when_equal(what_to_render, 2) + when_equal(what_to_render, 3) );
    
	//}
    //else if ( what_to_render == 3 ) // render both Qualitys
	//{   
        //thetaLightEach += thetaLight1 * when_equal(what_to_render, 3);
		//phiLightEach += phiLight1 * when_equal(what_to_render, 3);
		//thetaViewEach += thetaView1 * when_equal(what_to_render, 3);
		//phiViewEach += phiView1 * when_equal(what_to_render, 3);
        
        //col0 += calcColor(thetaView, phiView, thetaLight, phiLight, 0).xyz * when_equal(what_to_render, 3);

        //thetaLightEach += thetaLight2 * when_equal(what_to_render, 3);
		//phiLightEach += phiLight2 * when_equal(what_to_render, 3);
		//thetaViewEach += thetaView2 * when_equal(what_to_render, 3);
		//phiViewEach += phiView2 * when_equal(what_to_render, 3);
        
        //col1 += calcColor(thetaView, phiView, thetaLight, phiLight, 1).xyz * when_equal(what_to_render, 3);
	//}
    
    
	//	thetaLightEach = thetaLight1;
	//	phiLightEach = phiLight1;
	//	thetaViewEach = thetaView1;
	//	phiViewEach = phiView1;
    //  texture_code = 0;
        
    //    vec3 col0 = calcColor(thetaView, phiView, thetaLight, phiLight, 0).xyz;
    
	//	thetaLightEach = thetaLight2;
	//	phiLightEach = phiLight2;
	//	thetaViewEach = thetaView2;
	//	phiViewEach = phiView2;
    //    texture_code = 1;
        
    //    vec3 col1 = calcColor(thetaView, phiView, thetaLight, phiLight, 1).xyz;
    

    if( what_to_render == 0 ) // RETDOT
    {
        col = vec3(0.0, 1.0, 1.0);
        fragColor = vec4(col,1.0);

        //if (foveated == 1.0)
        //{
        //    col = vec3(1.0, 0.0, 0.0);
        //    fragColor = vec4(col,1.0);
        //}
        //else
        //{
        //    col = vec3(0.0, 1.0, 0.0);
         //   fragColor = vec4(col,1.0);
        //}
    }
    else if ( what_to_render == 1 )
    {
        fragColor = vec4(col0, 1.0);
    }
    else if( what_to_render == 2 )
    {
        fragColor = vec4(col1, 1.0);
    }
    if( what_to_render == 3 )   // else
    {
        float a = both_rad - sqrt(pow_distance); // a is the difference between radius_two (fading-ring) and the distance from the mouse to the Fragment (from Fragment to outer ring)
        

        col = vec3(col0 * (a / radius_two)) + vec3(col1 * ( 1 - (a / radius_two)));
        //fragColor = vec4( (a / radius_two), 0.0, 1.0 - (a / radius_two), 1.0);
        fragColor = vec4(col,1.0);
    }


}













