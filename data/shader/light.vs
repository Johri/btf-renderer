#version 420
//
layout(location = 0) in vec3 inPosition;
//
uniform mat4 vp;
uniform mat4 mvp;
//

void main()
{
	gl_Position = mvp * vec4(inPosition, 1.0);
	
	vec4 pos = vp * vec4(inPosition, 1.0);
	gl_PointSize = 100.0/length(pos.xyz);
}
