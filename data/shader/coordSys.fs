#version 420
//
in vec3 vPosition;
//
out vec4 fragColor;

void main()
{
	if (vPosition.x != 0.0)
	{
		fragColor = vec4(1.0, 0.0, 0.0, 1.0);	
	}
	else 
	if (vPosition.y != 0.0)
	{
		fragColor = vec4(1.0, 1.0, 0.0, 1.0);
	}
	else 
	if (vPosition.z != 0.0)
	{
		fragColor = vec4(0.0, 0.0, 1.0, 1.0);
	}
	else
	{
		fragColor = vec4(1.0, 1.0, 1.0, 1.0);
	}
	
}













