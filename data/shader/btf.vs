#version 420
//
layout(location = 0) in vec3 inPosition;
layout(location = 1) in vec3 inNormal;
layout(location = 2) in vec2 inTexCoord;
layout(location = 3) in vec3 inTangent;
layout(location = 4) in vec3 inBiTangent;
//
uniform mat4 mvp;
uniform mat4 mv;
//
out vec4 vPosition;
out vec2 vTexCoord;
out vec3 vNormalEye;
out vec3 vTangentEye;
out vec3 vBiTangentEye;
//out mat3 tbn;
//

void main()
{
	gl_Position = mvp * vec4(inPosition, 1.0);
	vPosition = mv * vec4(inPosition, 1.0);
	vTexCoord = inTexCoord;
	
	vNormalEye = ((mv * vec4(normalize(inNormal), 0.0)).xyz);
	vTangentEye = ((mv * vec4(normalize(inTangent), 0.0)).xyz);
	vBiTangentEye = ((mv * vec4(normalize(inBiTangent), 0.0)).xyz);
	
//	tbn = inverse(mat3(vTangentEye, vBiTangentEye, vNormalEye));
}
