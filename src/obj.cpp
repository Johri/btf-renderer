#include "obj.h"

#include <iostream>
#include <fstream>
#include <sstream>

Obj::Obj () : loaded_(false)
{}

Obj::Obj (std::string filename) : loaded_(false)
{
	load(filename);
}


bool Obj::isLoaded () const
{
	return loaded_;
}


bool Obj::load (std::string filename)
{
	if (filename != std::string())
	{
		objName = filename;
	}
	
	std::ifstream in(filename, std::ios::in);
	if (!in)
	{
		std::cerr << "Cannot open " << objName << std::endl;
		loaded_ = false;
		return false;
	}
	
	std::vector<glm::vec3> positionsTemp;
	std::vector<glm::vec3> normalsTemp;
	std::vector<glm::vec2> texcoordTemp;
		
	std::string line;
	while (getline(in, line)) 
	{
		// Vertices
		if (line.substr(0,2) == "v ")
		{
			std::istringstream s(line.substr(2));
			glm::vec3 v; 
			s >> v.x; 
			s >> v.y; 
			s >> v.z;
			positionsTemp.push_back(v);
		} 
		// Indices
		else if (line.substr(0,2) == "f ")
		{
			std::istringstream sstr(line.substr(2));
			
			std::vector<std::string> v;
			std::string sTemp;
			sstr >> sTemp;
			
			while (sTemp != "")
			{
				v.push_back(sTemp);
				sTemp = std::string();
				sstr >> sTemp;
			}
			
			for (unsigned int i = 0; i < v.size(); ++i)
			{
				unsigned int index[3] = {0};
				unsigned int counter = 0;
				std::string s = v[i];
				size_t oldPos = 0;
				size_t newPos = s.find_first_of ("/");
				while (newPos != std::string::npos)
				{
					// skips p//n faces
					if (newPos != oldPos)
					{
						std::string subs = s.substr(oldPos, newPos);
						index[counter] = static_cast<unsigned int>(std::stoul(subs));
					}
					++counter;
					oldPos = newPos+1;
					newPos = s.find_first_of ("/", oldPos);
				}
				std::string subs = s.substr(oldPos);
				index[counter] = static_cast<unsigned int>(std::stoul(subs));
				
				if (i <= 3)
				{
					if (index[0] != 0)
					{
						positions.push_back (positionsTemp[index[0]-1]);
					}
					if (index[1] != 0)
					{
						texCoords.push_back (texcoordTemp[index[1]-1]);
					}
					if (index[2] != 0)
					{
						normals.push_back (normalsTemp[index[2]-1]);
					}
				}
				else
				{
					if (index[0] != 0)
					{
						positions.push_back (positions[positions.size()-i+1]);
						positions.push_back (positions[positions.size()-1]);
						positions.push_back (positionsTemp[index[0]]);
					}
					if (index[1] != 0)
					{
						texCoords.push_back (texCoords[texCoords.size()-i+1]);
						texCoords.push_back (texCoords[texCoords.size()-1]);
						texCoords.push_back (texcoordTemp[index[1]]);
					}
					if (index[2] != 0)
					{
						normals.push_back (normals[normals.size()-i+1]);
						normals.push_back (normals[normals.size()-1]);
						normals.push_back (normalsTemp[index[2]]);
					}
				}
			}
		}
		// TexCoord
		else if (line.substr(0,3) == "vt ")
		{
			std::istringstream s(line.substr(3));
			glm::vec2 v;
			s >> v.x; 
			s >> v.y;
			texcoordTemp.push_back(v);
		}
		// Normals
		else if (line.substr(0,3) == "vn ")
		{
			std::istringstream s(line.substr(3));
			glm::vec3 v;
			s >> v.x; 
			s >> v.y; 
			s >> v.z;
			normalsTemp.push_back(v);
		}
		// Comment
		else if (line[0] == '#') 
		{
			/* ignoring this line */
		}
		else
		{
			/* ignoring this line */
		}
	}
	
	if (positions.size() == 0)
	{
		loaded_ = false;
		return false;
	}
	else if (positions.size() != texCoords.size() && texCoords.size() != 0)
	{
		loaded_ = false;
		return false;
	}
	else if (positions.size() != normals.size() && normals.size() != 0)
	{
		loaded_ = false;
		return false;
	}
	else
	{
		loaded_ = true;
		return true;
	}
}


std::vector<glm::vec3> const& Obj::getPositions() const
{
	return positions;
}


std::vector<glm::vec3> const& Obj::getNormals() const
{
	return normals;
}


std::vector<glm::vec2> const& Obj::getTexCoords() const
{
	return texCoords;
}


