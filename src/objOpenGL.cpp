#include "objOpenGL.h"


bool ObjOpenGL::loadObj (std::string filename, bool tangent)
{
	object_ = Obj (filename);
	if (object_.isLoaded() == false)
	{
		return false;
	}
	
	glDeleteVertexArrays (1, &vaoID_);
	glGenVertexArrays (1, &vaoID_);
	glBindVertexArray (vaoID_);
	{
		GLuint vboPos;
		GLuint vboNorm;
		GLuint vboTexC;
		GLuint vboTang;
		GLuint vboBitang;
		
		// positions
		std::vector<glm::vec3> const& pos = object_.getPositions();
		if (pos.size() == 0)
		{
			return false;
		}
		glGenBuffers (1, &vboPos);
		glBindBuffer (GL_ARRAY_BUFFER, vboPos);
		{
			glBufferData (GL_ARRAY_BUFFER, pos.size() * sizeof(glm::vec3), &pos[0], GL_STATIC_DRAW);
			glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, nullptr);
			glEnableVertexAttribArray(0);
		}
		
		// normals
		std::vector<glm::vec3> const& norm = object_.getNormals();
		if (norm.size() != 0)
		{
			glGenBuffers (1, &vboNorm);
			glBindBuffer (GL_ARRAY_BUFFER, vboNorm);
			{
				glBufferData (GL_ARRAY_BUFFER, norm.size() * sizeof(glm::vec3), &norm[0], GL_STATIC_DRAW);
				glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, nullptr);
				glEnableVertexAttribArray(1);
			}
		}
		
		// texcoords
		std::vector<glm::vec2> const& texC = object_.getTexCoords();
		if (texC.size() != 0)
		{
			glGenBuffers (1, &vboTexC);
			glBindBuffer (GL_ARRAY_BUFFER, vboTexC);
			{
				glBufferData (GL_ARRAY_BUFFER, texC.size() * sizeof(glm::vec2), &texC[0], GL_STATIC_DRAW);
				glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 0, nullptr);
				glEnableVertexAttribArray(2);
			}
		}
		
		if (tangent)
		{
			std::vector<glm::vec3> tangent;
			std::vector<glm::vec3> bitangent;
			calcTangent(pos, texC, tangent, bitangent);
			
			for (unsigned int i = 0; i < pos.size(); ++i)
			{
				glm::vec3 tempTan = glm::vec3(0.0f);
				glm::vec3 tempBiTan = glm::vec3(0.0f);
				unsigned int counter = 0;
				
				for (unsigned int j = 0; j < pos.size(); ++j)
				{
					if (pos[i] == pos[j])
					{
						tempTan += tangent[j];
						tempBiTan += bitangent[j];
						++counter;
					}
				}
				if (counter == 0)
				{
					std::cerr << "error float converting in obj, pos[i] != pos[j] with i == j" << std::endl;
					return false;
				}
//				tempTan /= counter;
//				tempBiTan /= counter;

				tangent[i] = tempTan;
				bitangent[i] = tempBiTan;
				
			}
			
			glGenBuffers (1, &vboTang);
			glBindBuffer (GL_ARRAY_BUFFER, vboTang);
			{
				glBufferData (GL_ARRAY_BUFFER, tangent.size() * sizeof(glm::vec3), &tangent[0], GL_STATIC_DRAW);
				glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, 0, nullptr);
				glEnableVertexAttribArray(3);
			}
			
			glGenBuffers (1, &vboBitang);
			glBindBuffer (GL_ARRAY_BUFFER, vboBitang);
			{
				glBufferData (GL_ARRAY_BUFFER, bitangent.size() * sizeof(glm::vec3), &bitangent[0], GL_STATIC_DRAW);
				glVertexAttribPointer(4, 3, GL_FLOAT, GL_FALSE, 0, nullptr);
				glEnableVertexAttribArray(4);
			}			
		}
	}
	
	
	return true;
}


void ObjOpenGL::calcTangent(std::vector<glm::vec3> const& pos, std::vector<glm::vec2> const& texCoord, std::vector<glm::vec3>& tangent, std::vector<glm::vec3>& bitangent)
{
	tangent.resize(pos.size());
	bitangent.resize(pos.size());
	
	for (unsigned int i = 0; i < pos.size(); i += 3)
	{
		glm::vec3 dPos1 = pos[i+1] - pos[i+0];
		glm::vec3 dPos2 = pos[i+2] - pos[i+0];
		
		glm::vec2 dUV1 = texCoord[i+1] - texCoord[i+0];
		glm::vec2 dUV2 = texCoord[i+2] - texCoord[i+0];
		
		float temp = 1.0f / (dUV1.x * dUV2.y - dUV1.y * dUV2.x);
		glm::vec3 tan = (dPos1 * dUV2.y - dPos2 * dUV1.y) * temp;
		glm::vec3 bitan = (dPos2 * dUV1.x - dPos1 * dUV2.x) * temp;
		tangent[i+0] = tan;
		tangent[i+1] = tan;
		tangent[i+2] = tan;
		bitangent[i+0] = bitan;
		bitangent[i+1] = bitan;
		bitangent[i+2] = bitan;
	}
}

void ObjOpenGL::draw ()
{
	glBindVertexArray (vaoID_);
	
		glDrawArrays(GL_TRIANGLES, 0, object_.getPositions().size());
		
	glBindVertexArray (0);
}









