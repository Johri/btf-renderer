#include <AntTweakBar.h>
#include "gl_core_4_2.h"
#include "utility.h"
#include "gl_util.h"
#include "objOpenGL.h"

#include "glm/glm.hpp"
#include "glm/ext.hpp"
#include <GL/freeglut.h>

#include <sstream> // stringstream
#include <iomanip>
#include <math.h>
#include <cmath>
#include <string>
#include <vector>
#include <fstream>
#include <sstream>
#include <map>

#include <direct.h>

#include <windows.h>
#include <glext.h> // switch of vsync for performance testing
#include <wglext.h>


#define PI 3.14159265359




#define BUFFER_OFFSET(i) ((char *)nullptr + (i))

namespace
{
	std::string const applicationName("BTF-Texturing");

	std::string const btfVertShaderSource("data/shader/btf.vs");
	std::string const btfFragShaderSource("data/shader/btf.fs");
	std::string const lightVertShaderSource("data/shader/light.vs");
	std::string const lightFragShaderSource("data/shader/light.fs");
	std::string const coordSysVertShaderSource("data/shader/coordSys.vs");
	std::string const coordSysFragShaderSource("data/shader/coordSys.fs");

	int windowWidth(1680);
	int viewportWidth = windowWidth;
	int windowHeight(1050);
	float nearPlane = 1.0f;
	float farPlane = 1000.0f;
	glm::vec4 backgroundColor(0.705f, 0.705f, 0.705f, 1.0f);

	int const openglMajorVersion(4);
	int const openglMinorVersion(2);

	glm::mat4 modelMatrix(1.0f);
	glm::mat4 viewMatrix(1.0f);
	glm::mat4 projectionMatrix(1.0f);
	glm::mat4 modelMatrixLight(1.0f);

	int textureOffset; //Martin Johr

	int lastButton;
	glm::vec3 lastMousePos;
	glm::vec3 mousi; //Martin Johr
	glm::vec3 center_; //Martin Johr
	unsigned int radius_two = 100; //Martin Johr
	bool foveated = false; //Martin Johr
	bool interval_switch = false;
	bool switchMouseFunc = true;
	bool switchPOI = true; //Martin Johr
	bool switchLightMoveMode = true;
	bool switchRotZoomMode = true;
	bool showLightBulb = true;
	bool showCoordSys_ = true;
	long long screenshotCounter = 0;

	// Martin Johr
	// Timer and FPS
	float fps;
	int frames;
	int frametime;
	int base_frametime;

	// Martin Johr
	// Foveation Setup
	float UserScreenDistance_mm = 700;
	float ScreenWidth_mm = 532;
	float ScreenWidth_px = 1920;
	float VisionSpanDiameter_deg = 5;
	float mm_per_cycle = 4;
	float Radius_mm;
	int Radius_px;
	float CyclesPerDegree;
	float MillimeterPerDegree;

	GLuint btfProgramID;
	GLuint lightProgramID;
	GLuint coordSysProgramID;

	GLuint vaoLightID;
	GLuint vboLightID;
	GLuint vaoCoordSysID;
	GLuint vboCoordSysID;
	std::vector<std::vector<GLuint> > texID(2);

	ObjOpenGL obj;
	std::string objPath("data/obj/");
	char objFile[64] = "plane3.obj";
	int objCount = 1;

	bool const compress = true;
	std::string textureListFile("list.txt");
	std::string texturePath("data/textures/");
	//	int const texSize = 256;
	//	int const logTexSize = int(std::log10(float(texSize))/std::log10(2.0f));
	char texObject[2][64] = { "cord", "cord" };
	char foldername[64] = "final cord light -30x 17z";
	int texCounter = 0;
	//	if theta interval != 15 -> modify shader
	float phiInterval[6] = { 360.0f, 60.0f, 30.0f, 20.0f, 18.0f, 15.0f };
	float phiIntervalCount[7] = { 0.0f, 1.0f, 7.0f, 19.0f, 37.0f, 57.0f, 81.0f };
	unsigned int texPerViewDir = 81;
	float thetaInterval = 15.0f;
	float textureScale = 7.0f;
	bool mipmappinglo = true;
	bool mipmappingro = true;
	GLuint samplers[2] = { 0 };
	int maxTexLayers = 0;


	TwBar* gui;
	typedef enum { X32 = 32, X50 = 50, X48 = 48, X64 = 64, X75 = 75, X96 = 96, X100 = 100, X128 = 128, X150 = 150, X200 = 200, X256 = 256, X400 = 400, X512 = 512 } TextureSize;
	TwEnumVal tsEV[] = { { X32, "32" }, { X48, "48" }, { X50, "50" }, { X64, "64" }, { X75, "75" }, { X96, "96" }, { X100, "100" }, { X128, "128" }, { X150, "150" }, { X200, "200" }, { X256, "256" }, { X400, "400" }, { X512, "512" } };
	TextureSize texSize[2] = { X256, X256 };

	typedef enum { JPG = 0, RAW = 1, BMP = 2 } ImageType;
	TwEnumVal itEV[] = { { JPG, "*.jpg" }, { RAW, "*.raw" }, { BMP, "*.bmp" } };
	ImageType imgType[2] = { JPG, JPG };

	typedef enum { FRST = 1, SCND, THRD, FRTH, FFTH, SXTH } TextureFrequ;
	TwEnumVal tEV[] = { { FRST, "1." }, { SCND, "2." }, { THRD, "3." }, { FRTH, "4." }, { FFTH, "5." }, { SXTH, "6." } };
	TextureFrequ thetalight1 = FRST;
	TextureFrequ philight1 = FRST;
	TextureFrequ thetalight2 = FRST;
	TextureFrequ philight2 = FRST;
	TextureFrequ thetaview1 = FRST;
	TextureFrequ phiview1 = FRST;
	TextureFrequ thetaview2 = FRST;
	TextureFrequ phiview2 = FRST;

	typedef enum { no_smoothing = 0, random, linear_interpolated_random, linear_interpolated} SmoothSwitch;
	TwEnumVal swEV[] = { { no_smoothing, "no smoothing" }, { random, "random" }, { linear_interpolated_random, "linear interpolated random" }, { linear_interpolated, "linear interpolated" } };
	SmoothSwitch Smooth_Switch = linear_interpolated;

	glm::vec4 rotation(0.0f, 0.0f, 0.0f, 1.0f);
	float zoom = -10.5;
	float shiftx = 0.013;
	float shifty = 0.006;
	glm::vec3 lightPosition(0.0f, 0.0f, 15.0f);

} //namespace

bool initDebugOutput();
bool initProgram(GLuint& program, std::string const& vertShaderSource, std::string const& fragShaderSource);
bool initLight();
bool initCoordSys();
bool initTexture();
bool initSamplers();
bool initMatrices();
bool initObj();
void saveImagePNG(); // Martin Johr
bool initAntTweakBar();
void setTexUniforms();
void setMatrixUniforms();
void setUniforms();
void setThetaLight2x(TextureFrequ newVal);
void setPhiLight2x(TextureFrequ newVal);
void setThetaView2x(TextureFrequ newVal);
void setPhiView2x(TextureFrequ newVal);
void setTexScale1x(int newVal);
void setTexScale2x(int newVal);
void setVisionSpanDiameter_degx(int newVal);
void setSmoothSwitchx(SmoothSwitch newVal);
void reload(void*);
void display();
bool cleanupGL();
void calculateRadius_mm(); // Martin Johr
void calculateRadius_px(); // Martin Johr
void calculate_CyclesPerDegree(); // Martin Johr
void calculate_MillimeterPerDegree(); // Martin Johr
void calculateCenter(); // Martin Johr
void key(unsigned char key, int x, int y);
void specialKey(int key, int x, int y);
void motionFunc(int x, int y);
void mouseFunc(int button, int state, int x, int y);
void mousimove(int x, int y); // Martin Johr
void resize(int width, int height);
bool initGL();
bool initGLUT(int argc, char* argv[]);



bool initDebugOutput()
{
	bool validated = true;

	glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS_ARB);
	glDebugMessageControlARB(GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0, nullptr, GL_TRUE);
	glDebugMessageCallbackARB(debugOutput, nullptr);

	return validated;
}


bool initProgram(GLuint& program, std::string const& vertShaderSource, std::string const& fragShaderSource)
{
	bool validated = true;
	base_frametime = glutGet(GLUT_ELAPSED_TIME);
	{
		GLuint vsShader = glCreateShader(GL_VERTEX_SHADER);
		GLuint fsShader = glCreateShader(GL_FRAGMENT_SHADER);

		std::ifstream vsFile(vertShaderSource);
		std::string vsSource((std::istreambuf_iterator<char>(vsFile)), std::istreambuf_iterator<char>());
		const char* vsSourceTemp = vsSource.c_str();
		glShaderSource(vsShader, 1, &vsSourceTemp, 0);
		glCompileShader(vsShader);
		validated = validated && printShaderInfoLog(vsShader);

		std::ifstream fsFile(fragShaderSource);
		std::string fsSource((std::istreambuf_iterator<char>(fsFile)), std::istreambuf_iterator<char>());
		const char* fsSourceTemp = fsSource.c_str();
		glShaderSource(fsShader, 1, &fsSourceTemp, 0);
		glCompileShader(fsShader);
		validated = validated && printShaderInfoLog(fsShader);

		program = glCreateProgram();

		glAttachShader(program, vsShader);
		glAttachShader(program, fsShader);

		glLinkProgram(program);
		validated = validated && printProgramInfoLog(program);

		glDeleteShader(vsShader);
		glDeleteShader(fsShader);
	}

	return validated;
}


bool initLight()
{
	bool validated = true;

	glm::vec4 pl(0.0f, 0.0f, 0.0f, 1.0f);
	glGenBuffers(1, &vboLightID);
	glBindBuffer(GL_ARRAY_BUFFER, vboLightID);
	glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec4), glm::value_ptr(pl), GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);


	glGenVertexArrays(1, &vaoLightID);
	glBindVertexArray(vaoLightID);
	{
		glBindBuffer(GL_ARRAY_BUFFER, vboLightID);

		glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(0);
	}
	glBindVertexArray(0);

	return validated;
}


bool initCoordSys()
{
	bool validated = true;

	std::vector<glm::vec3> kos;
	kos.push_back(glm::vec3(0.0f, 0.0f, 0.0f));
	kos.push_back(glm::vec3(1.0f, 0.0f, 0.0f));
	kos.push_back(glm::vec3(0.0f, 0.0f, 0.0f));
	kos.push_back(glm::vec3(0.0f, 1.0f, 0.0f));
	kos.push_back(glm::vec3(0.0f, 0.0f, 0.0f));
	kos.push_back(glm::vec3(0.0f, 0.0f, 1.0f));


	glGenBuffers(1, &vboCoordSysID);
	glBindBuffer(GL_ARRAY_BUFFER, vboCoordSysID);
	glBufferData(GL_ARRAY_BUFFER, 6 * sizeof(glm::vec4), kos.data(), GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);


	glGenVertexArrays(1, &vaoCoordSysID);
	glBindVertexArray(vaoCoordSysID);
	{
		glBindBuffer(GL_ARRAY_BUFFER, vboCoordSysID);

		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(0);
	}
	glBindVertexArray(0);

	return validated;
}

// uploading textures and mipmaps // current use case: upload one texture-package first (first run), then upload second texture-package (second run). each package contains 6561 textures. which texture is getting uploaded depends on objID (0 or 1)
bool initTexture(unsigned int texNum)
{
	bool validated = true;

	/*
	GLint max_layer;
	glGetIntegerv(GL_MAX_VERTEX_TEXTURE_IMAGE_UNITS, &max_layer);	// vertex shader
	std::cout << "GL_MAX_VERTEX_TEXTURE_IMAGE_UNITS: " << max_layer << std::endl;

	glGetIntegerv(GL_MAX_TEXTURE_IMAGE_UNITS, &max_layer);	// fragment shader
	std::cout << "GL_MAX_TEXTURE_IMAGE_UNITS: " << max_layer << std::endl;

	glGetIntegerv(GL_MAX_GEOMETRY_TEXTURE_IMAGE_UNITS, &max_layer);	// geometry shader
	std::cout << "GL_MAX_GEOMETRY_TEXTURE_IMAGE_UNITS: " << max_layer << std::endl;

	glGetIntegerv(GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS, &max_layer);	// combined
	std::cout << "GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS: " << max_layer << std::endl;

	glGetIntegerv(GL_MAX_ARRAY_TEXTURE_LAYERS, &max_layer);
	std::cout << "GL_MAX_ARRAY_TEXTURE_LAYERS: " << max_layer << std::endl;
	*/


	// set path from where to load the texture
	std::string path(texturePath + std::to_string(static_cast<long long>(texSize[texNum])) + 'x' + std::to_string(static_cast<long long>(texSize[texNum])) + '/' + texObject[texNum] + '/');

	std::cout << "loading Texture: " << texNum << "." << std::endl;

	std::cout << "Initializing BTF-data for \"" << texObject[texNum] << "\", size: " << texSize[texNum] << "x" << texSize[texNum] << " (Object " << texNum << ")" << std::endl;

	if (texSize[texNum] == 32 || texSize[texNum] == 64 || texSize[texNum] == 128 || texSize[texNum] == 256)
	{
		textureListFile = "list.txt";
		textureOffset = int(6561 / 2048) + 1;

		GLuint TexOffsetLoc = glGetUniformLocation(btfProgramID, "textureOffset");
		glProgramUniform1i(btfProgramID, TexOffsetLoc, textureOffset);

		interval_switch = false;
	}
	else if (texSize[texNum] == 50 || texSize[texNum] == 100 || texSize[texNum] == 200 || texSize[texNum] == 400)
	{
		textureListFile = "list3.txt";
		textureOffset = int(22801 / 2048) + 1;

		GLuint TexOffsetLoc = glGetUniformLocation(btfProgramID, "textureOffset");
		glProgramUniform1i(btfProgramID, TexOffsetLoc, textureOffset);

		interval_switch = true;
	}
	else
	{
		std::cout << "folder " + std::to_string(texSize[texNum]) + "x" + std::to_string(texSize[texNum]) + " not found" << std::endl;
	}
	
	//	validated = initProgram(btfProgramID, btfVertShaderSource, btfFragShaderSource);

	// fill ifstream with textureListFile (data\textures\list.txt). list.txt holds name of each texture and in which folder to find.
	std::ifstream in(texturePath + textureListFile, std::ios::in);
	if (!in)
	{
		std::cerr << "Cannot open " << texturePath + textureListFile << std::endl;
		validated = false;
		return validated;
	}

	// counting the number of textures given in list.txt
	std::string line;
	int counter = 0;
	while (getline(in, line))
	{
		++counter;
	}
	in.clear();
	in.seekg(0, std::ifstream::beg); // go back to beginning position in ifstream

	//int max = GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS; // Martin Johr

	glGetIntegerv(GL_MAX_ARRAY_TEXTURE_LAYERS, &maxTexLayers); // checking "..maximum number of layers allowed in an array texture.." on the current graficscard https://www.khronos.org/registry/OpenGL-Refpages/gl4/html/glGet.xhtml GL_MAX_ARRAY_TEXTURE_LAYERS
	int arraySize = static_cast<int>(std::ceil(float(counter) / 2048.0f)); // calculating the necessarry size of the sampler2DArray texArray[?] -> 6561/2048 = 3,2 -> ceil(3,2) = 4
	if (maxTexLayers < 2048)
	{
		std::cerr << "Number of texture layers to high for this graphic card.\nmax number: " << maxTexLayers << "\nrequested number: " << arraySize << "x2048" << std::endl;
		return false;
	}

	glPixelStorei(GL_UNPACK_ALIGNMENT, 1); // ??? RELEVANT ??? HAVE NO IDEA https://www.khronos.org/registry/OpenGL-Refpages/es2.0/xhtml/glPixelStorei.xml -> uploading textures will be bitwise
	texID[texNum].resize(arraySize); // texID[texNum] is std::vector<GLuint> -> resize to arraysize "4"
	glGenTextures(arraySize, &texID[texNum][0]); // generates values/"Textures"(not sure) of type Gluint starting from 2 to 5 (... from 6 to 9 if second texture(texID) array gets uploaded)
	std::cout << "Allocating texture storage...";
	for (unsigned int i = 0; i < texID[texNum].size(); ++i) // loop of 4 (6561 / 2048)
	{
		glActiveTexture(GL_TEXTURE0 + i + texNum * 4); // maybe activates texture 0 to 3, because loop of 4. will be bound to Gluint in texID. Maybe the bond will stay and following glTexParameteri get relevant for the whole layer. 
		glBindTexture(GL_TEXTURE_2D_ARRAY, texID[texNum][i]); // bind the Gluint from value 2 to 5 (first texture) or 6 to 9 (second texture). More important is the position in the field texID[texNum][X] considering the counterTa later. It specifies the layer in storage.
		{
			glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_S, GL_REPEAT);
			glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_T, GL_REPEAT);
			glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
			glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			if (compress)
			{
				glTexImage3D(GL_TEXTURE_2D_ARRAY, 0, GL_COMPRESSED_RGB_S3TC_DXT1_EXT, texSize[texNum], texSize[texNum], 2048, 0, GL_RGB, GL_UNSIGNED_BYTE, nullptr); // Prepare storage (width x hight of texture x 2048(2048 is maximum size of each layer))
			}
			else
			{
				glTexImage3D(GL_TEXTURE_2D_ARRAY, 0, GL_RGB, texSize[texNum], texSize[texNum], 2048, 0, GL_RGB, GL_UNSIGNED_BYTE, nullptr); // Prepare storage (width x hight of texture x 2048(2048 is maximum size of each layer))
			}
		}
	}
	std::cout << "...done" << std::endl;

	counter = 0; // counter to index the texture inside a layer (from 0 to 2047)
	int counterTa = 0; // counter to index each layer
	glActiveTexture(GL_TEXTURE0 + texNum * 4); // activates the very first texture - in the first layer (uniform sampler2DArray texArray[0]) ALTERNATIV -> (GL_TEXTURE0 + 6561 / 2048 * texNum); calculation for offset is wrong. must use (GL_TEXTURE0 + ceil(6561 / 2048) * objID) it will index to the right layer, but there will be a gab if mod(6561, 2048) is not 0. to overcome: must use offset of texture (mod(6561, 2048)) and layeroffset (which is the current layer (div(6561, 2048))) changing has to be made for each glActiveTexture.
	glBindTexture(GL_TEXTURE_2D_ARRAY, texID[texNum][0]); // GL_TEXTURE_2D_ARRAY is not bound to texture but to a GLuint. it may specify the first texture in the first layer or the whole first layer.
	std::cout << "Loading texture data";
	while (getline(in, line)) // go through list.txt
	{
		std::stringstream sstr(line); // get current line(texture adress and name)

		std::vector<char> image;
		if (imgType[texNum] == JPG) // choose given texture type
		{
			image = readJpegImage(path + std::move(line));
		}
		else if (imgType[texNum] == RAW)
		{
			image = readRawImage(path + std::move(line));
		}
		else if (imgType[texNum] == BMP)
		{
			image = readBmpImage(path + std::move(line));
		}
		else
		{
			std::cerr << "\nerror: invalid image type, cannot read textures" << std::endl;
			return false;
		}

		if (image.size() == 0) // image not found or empty
		{
			return false;
		}

		// 2D array textures are created similarly; bind a newly-created texture object to GL_TEXTURE_2D_ARRAY, then use the "3D" image functions to allocate storage. The depth parameter sets the number of layers in the array. https://www.khronos.org/opengl/wiki/Array_Texture
		glTexSubImage3D(GL_TEXTURE_2D_ARRAY, 0, 0, 0, counter, texSize[texNum], texSize[texNum], 1, GL_RGB, GL_UNSIGNED_BYTE, image.data()); // uploading texture into sampler2DArray texArray[counterTa]
		++counter;

		if (counter == 2048)
		{
			counter = 0;
			++counterTa;
			glActiveTexture(GL_TEXTURE0 + counterTa + texNum * 4); // activates the first texture in new layer or the whole new layer (uniform sampler2DArray texArray[counterTa]) ALTERNATIV -> (GL_TEXTURE0 + counterTa + 6561 / 2048 * objID);
			//std::cout << "?" << std::endl;
			glBindTexture(GL_TEXTURE_2D_ARRAY, texID[texNum][counterTa]); // bind new layer
		}
		if (counter % 100 == 0)
		{
			std::cout << "." << std::flush;
		}
	}
	std::cout << "done" << std::endl;

	std::cout << "Generating mipmaps";
	for (unsigned int i = 0; i < texID[texNum].size(); ++i)
	{
		std::cout << "." << std::flush;
		glActiveTexture(GL_TEXTURE0 + i + texNum * 4); // same as before // index layers
		glBindTexture(GL_TEXTURE_2D_ARRAY, texID[texNum][i]); // don't know why always both active and bind need to be called every time. GL_TEXTURE0 + i should already and still bound to texID[objID][i]. Is it really necessary? if i change it to texID[objID][i+1] or even more different it could cause trubble, I think.
		glGenerateMipmap(GL_TEXTURE_2D_ARRAY);
	}
	std::cout << "done\n" << "Initialization of BTF-data complete." << std::endl;


	glPixelStorei(GL_UNPACK_ALIGNMENT, 4); // uploading is finished -> change to GL_UNPACK_ALIGNMENT wordwise. maybe necessarry for rendering or reading.

	validated = validated && printProgramInfoLog(btfProgramID);

	return validated;
}
 
bool initSamplers()
{
	bool validated = true;

	glGenSamplers(2, &samplers[0]);
	glSamplerParameteri(samplers[0], GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glSamplerParameteri(samplers[1], GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

	return validated;
}

bool initMatrices()
{
	bool validated = true;

	modelMatrix = glm::mat4(1.0f);
	viewMatrix = glm::lookAt(glm::vec3(0.0f, 0.0f, 3.0f),
		glm::vec3(0.0f, 0.0f, 0.0f),
		glm::vec3(0.0f, 1.0f, 0.0f));

	projectionMatrix = glm::perspective(90.0f, float(viewportWidth) / float(windowHeight), nearPlane, farPlane);
	modelMatrixLight = glm::translate(glm::mat4(1.0f), glm::vec3(5.0f, 3.0f, 0.0f));

	setMatrixUniforms();

	return validated;
}


bool initObj()
{
	bool validated = true;

	validated = obj.loadObj(objPath + objFile, true);

	return validated;
}


// stackoverflow: Portable way to check if directory exists [Windows/Linux, C] https://stackoverflow.com/questions/18100097/portable-way-to-check-if-directory-exists-windows-linux-c
BOOL DirectoryExists(LPCTSTR szPath)
{
	DWORD dwAttrib = GetFileAttributes(szPath);

	return (dwAttrib != INVALID_FILE_ATTRIBUTES &&
		(dwAttrib & FILE_ATTRIBUTE_DIRECTORY));
}
//


void TW_CALL createTestImages(void*)
{
	screenshotCounter = 0;

	std::cout << "start creating test images" << std::endl;
	std::cout << "write to folder" << foldername << std::endl;

	//std::vector<int> scalings = { /*6, 7, 8, 9, 10, 11, */12 };
	std::vector<int> VSD_d = { 10, 20/*, 30*/ };

	std::vector<TextureFrequ> thetas = { FRST, SCND/*, THRD */};
	std::vector<TextureFrequ> phis = { FRST, SCND/*, THRD, FRTH, FFTH, SXTH */};
	std::vector<std::vector<TextureFrequ>> phithas;

	std::vector<SmoothSwitch> checksmooth = { /*no_smoothing, random,*/ linear_interpolated, linear_interpolated_random };

	phithas.push_back(thetas);
	phithas.push_back(phis);
	phithas.push_back(thetas);
	phithas.push_back(phis);

	int enumSize = 3;

	switchPOI = false;

	setSmoothSwitchx(no_smoothing);

	setThetaLight2x(FRST);
	setPhiLight2x(FRST);
	setThetaView2x(FRST);
	setPhiView2x(FRST);

	//for (unsigned int sca = 0; sca < scalings.size(); sca++)
	//{	// set the scale

		//setTexScale1x(scalings[sca]);

		if (texSize[0] == 32 || texSize[0] == 64 || texSize[0] == 128 || texSize[0] == 256)
		{
			std::vector<TextureSize> texSizeArray = { X256, /*X128,*/ X96, X64, X48, X32};

			for (unsigned int texi = 0; texi < 1; texi++)
			{
				texSize[0] = texSizeArray[texi];
				initTexture(0);

				for (unsigned int texj = texi; texj < texSizeArray.size(); texj++)
				{
					texSize[1] = texSizeArray[texj];
					initTexture(1);

					if ( texi == 0 && texj == 0 || texi == 1 && texj == 1)
					{

						//Print the reference (angles are 1,1,1,1)
						foveated = false;
						saveImagePNG();

						foveated = true;

						setThetaLight2x(FRST);
						setPhiLight2x(FRST);
						setThetaView2x(FRST);
						setPhiView2x(FRST);

						for (int angle_1 = 0; angle_1 < phithas.size(); angle_1++)
						{	// reset the angles

							for (int downsampling_1 = 1; downsampling_1 < phithas[angle_1].size(); downsampling_1++)
							{	// Set the curent looped angle

								switch (angle_1)
								{
								case 0: setThetaLight2x(phithas[angle_1][downsampling_1]); break;
								case 1: setPhiLight2x(phithas[angle_1][downsampling_1]); break;
								case 2: setThetaView2x(phithas[angle_1][downsampling_1]); break;
								case 3: setPhiView2x(phithas[angle_1][downsampling_1]); break;
								}

								for (unsigned int VSDi = 0; VSDi < 1; VSDi++) // VSD_d.size(); VSDi++)
								{	// set VisionSpanDiameter_deg

									setVisionSpanDiameter_degx(VSD_d[VSDi]);
									for (int cs = 0; cs < 1; cs++) // checksmooth.size(); cs++)
									{	// set the smoothing style

										setSmoothSwitchx(checksmooth[cs]);

										saveImagePNG(); // and print
									}
									setSmoothSwitchx(checksmooth[0]);
								}

								for (int angle_2 = angle_1 + 1; angle_2 < phithas.size(); angle_2++)
								{	// Set the curent comined looped angle

									for (int downsampling_2 = 1; downsampling_2 < phithas[angle_2].size(); downsampling_2++)
									{	// Set the curent comined looped angle

										switch (angle_2)
										{
										case 0: setThetaLight2x(phithas[angle_2][downsampling_2]); break;
										case 1: setPhiLight2x(phithas[angle_2][downsampling_2]); break;
										case 2: setThetaView2x(phithas[angle_2][downsampling_2]); break;
										case 3: setPhiView2x(phithas[angle_2][downsampling_2]); break;
										}

										for (unsigned int VSDi = 0; VSDi < 1; VSDi++) // VSD_d.size(); VSDi++)
										{	// set VisionSpanDiameter_deg

											setVisionSpanDiameter_degx(VSD_d[VSDi]);
											for (int cs = 0; cs < 1; cs++) // checksmooth.size(); cs++)
											{	// set the smoothing style

												setSmoothSwitchx(checksmooth[cs]);

												saveImagePNG(); // and print
											}
											setSmoothSwitchx(checksmooth[0]);
										}
									}

									switch (angle_2)
									{
									case 0: setThetaLight2x(phithas[angle_2][0]); break;
									case 1: setPhiLight2x(phithas[angle_2][0]); break;
									case 2: setThetaView2x(phithas[angle_2][0]); break;
									case 3: setPhiView2x(phithas[angle_2][0]); break;
									}
								}
							}

							switch (angle_1)
							{
							case 0: setThetaLight2x(phithas[angle_1][0]); break;
							case 1: setPhiLight2x(phithas[angle_1][0]); break;
							case 2: setThetaView2x(phithas[angle_1][0]); break;
							case 3: setPhiView2x(phithas[angle_1][0]); break;
							}
						}
						// reset the angles for the resolution foveated images
						setThetaLight2x(FRST);
						setPhiLight2x(FRST);
						setThetaView2x(FRST);
						setPhiView2x(FRST);
					}
					
					
					
					if ( texi != texj )
					{	// Print the foveated pictures
						
						for (unsigned int VSDi = 0; VSDi < VSD_d.size(); VSDi++)
						{	// set VisionSpanDiameter_deg

							setVisionSpanDiameter_degx(VSD_d[VSDi]);
							for (int cs = 0; cs < checksmooth.size(); cs++)
							{	// set the smoothing style

								setSmoothSwitchx(checksmooth[cs]);

								saveImagePNG(); // and print
							}
							setSmoothSwitchx(checksmooth[no_smoothing]);
						}
					}
				}
			}
		}
		else if (texSize[0] == 50 || texSize[0] == 100 || texSize[0] == 200 || texSize[0] == 400)
		{
			std::vector<TextureSize> texSizeArray = { X400, X200, X150, X100, X75, /*X50*/ };

			for (unsigned int texi = 0; texi < 1; texi++)
			{
				texSize[0] = texSizeArray[texi];
				initTexture(0);

				for (unsigned int texj = texi; texj < texSizeArray.size(); texj++)
				{
					texSize[1] = texSizeArray[texj];
					initTexture(1);

					if (texi == 0 && texj == 0 || texi == 1 && texj == 1)
					{

						//Print the reference (angles are 1,1,1,1)
						foveated = false;
						saveImagePNG();

						foveated = true;

						setThetaLight2x(FRST);
						setPhiLight2x(FRST);
						setThetaView2x(FRST);
						setPhiView2x(FRST);

						for (int angle_1 = 0; angle_1 < phithas.size(); angle_1++)
						{	// reset the angles

							for (int downsampling_1 = 1; downsampling_1 < phithas[angle_1].size(); downsampling_1++)
							{	// Set the curent looped angle

								switch (angle_1)
								{
								case 0: setThetaLight2x(phithas[angle_1][downsampling_1]); break;
								case 1: setPhiLight2x(phithas[angle_1][downsampling_1]); break;
								case 2: setThetaView2x(phithas[angle_1][downsampling_1]); break;
								case 3: setPhiView2x(phithas[angle_1][downsampling_1]); break;
								}

								for (unsigned int VSDi = 0; VSDi < 1; VSDi++) // VSD_d.size(); VSDi++)
								{	// set VisionSpanDiameter_deg

									setVisionSpanDiameter_degx(VSD_d[VSDi]);
									for (int cs = 0; cs < 1; cs++) // checksmooth.size(); cs++)
									{	// set the smoothing style

										setSmoothSwitchx(checksmooth[cs]);

										saveImagePNG(); // and print
									}
									setSmoothSwitchx(checksmooth[0]);
								}

								for (int angle_2 = angle_1 + 1; angle_2 < phithas.size(); angle_2++)
								{	// Set the curent comined looped angle

									for (int downsampling_2 = 1; downsampling_2 < phithas[angle_2].size(); downsampling_2++)
									{	// Set the curent comined looped angle

										switch (angle_2)
										{
										case 0: setThetaLight2x(phithas[angle_2][downsampling_2]); break;
										case 1: setPhiLight2x(phithas[angle_2][downsampling_2]); break;
										case 2: setThetaView2x(phithas[angle_2][downsampling_2]); break;
										case 3: setPhiView2x(phithas[angle_2][downsampling_2]); break;
										}

										for (unsigned int VSDi = 0; VSDi < 1; VSDi++) // VSD_d.size(); VSDi++)
										{	// set VisionSpanDiameter_deg

											setVisionSpanDiameter_degx(VSD_d[VSDi]);
											for (int cs = 0; cs < 1; cs++) // checksmooth.size(); cs++)
											{	// set the smoothing style

												setSmoothSwitchx(checksmooth[cs]);

												saveImagePNG(); // and print
											}
											setSmoothSwitchx(checksmooth[0]);
										}
									}

									switch (angle_2)
									{
									case 0: setThetaLight2x(phithas[angle_2][0]); break;
									case 1: setPhiLight2x(phithas[angle_2][0]); break;
									case 2: setThetaView2x(phithas[angle_2][0]); break;
									case 3: setPhiView2x(phithas[angle_2][0]); break;
									}
								}
							}

							switch (angle_1)
							{
							case 0: setThetaLight2x(phithas[angle_1][0]); break;
							case 1: setPhiLight2x(phithas[angle_1][0]); break;
							case 2: setThetaView2x(phithas[angle_1][0]); break;
							case 3: setPhiView2x(phithas[angle_1][0]); break;
							}
						}
						// reset the angles for the resolution foveated images
						setThetaLight2x(FRST);
						setPhiLight2x(FRST);
						setThetaView2x(FRST);
						setPhiView2x(FRST);
					}



					if ( texi != texj )
					{	// Print the foveated pictures

						for (unsigned int VSDi = 0; VSDi < VSD_d.size(); VSDi++)
						{	// set VisionSpanDiameter_deg

							setVisionSpanDiameter_degx(VSD_d[VSDi]);
							for (int cs = 0; cs < checksmooth.size(); cs++)
							{	// set the smoothing style

								setSmoothSwitchx(checksmooth[cs]);

								glutPostRedisplay(); // to ensure that the display matches the bmp
								display();  display();

								saveImagePNG(); // and print
							}
							setSmoothSwitchx(checksmooth[no_smoothing]);
						}
					}
				}
			}
		}
	//}
	std::cout << "fertig" << std::endl;
}

void saveImagePNG()
{
	//bool current_foveated = foveated;
#define CreateDirectory  CreateDirectoryW
	//foveated = false;

	TwDefine(" gui visible=false ");

	bool current_showCoordSys_ = showCoordSys_;
	showCoordSys_ = false;

	glutPostRedisplay(); // to ensure that the display matches the bmp
	display();  display();

	std::string filepath("data/test_pictures/");

	filepath.append(foldername);

	std::string filename("");

	if (!DirectoryExists(filepath.c_str()))
	{
		std::cout << "folder " << foldername << "does not exist" << std::endl;
		std::cout << "-> create folder " << foldername << std::endl;
		CreateDirectoryA(filepath.c_str(), NULL);
	}

	filepath.append("/");

	std::stringstream precision_stream;
	std::string precision_string;

	if (foveated)
	{
		if (screenshotCounter < 10) filename += "0";
		filename += std::to_string(screenshotCounter);
		filename += " ";
		filename += texObject[0];
		filename += ", fov, ";
		filename += std::to_string(texSize[0]);
		filename += "x";
		filename += std::to_string(texSize[0]);
		filename += ", ";
		filename += std::to_string(texSize[1]);
		filename += "x";
		filename += std::to_string(texSize[1]);
		filename += ", ";

		precision_stream << std::fixed << std::setprecision(1) << VisionSpanDiameter_deg;
		precision_string = precision_stream.str();

		filename += precision_string;

		precision_stream.str("");
		precision_string = precision_stream.str();

		filename += "�, scale ";
		precision_stream << std::fixed << std::setprecision(0) << textureScale;
		precision_string = precision_stream.str();
		filename += precision_string;

		filename += ", thli ";
		filename += std::to_string(thetalight2);

		filename += ", phli ";
		filename += std::to_string(philight2);

		filename += ", thvi ";
		filename += std::to_string(thetaview2);

		filename += ", phvi ";
		filename += std::to_string(phiview2);

		filename += ", ";
		if (Smooth_Switch == 0)
		{
			filename += "no smoothing";
		}
		else if (Smooth_Switch == 1)
		{
			filename += "random";
		}
		else if (Smooth_Switch == 2)
		{
			filename += "interpolated random";
		}
		else if (Smooth_Switch == 3)
		{
			filename += "interpolated";
		}
	}
	else
	{
		if (screenshotCounter < 10) filename += "0";
		filename += std::to_string(screenshotCounter);
		filename += " ";
		filename += texObject[0];
		filename += ", ref, ";
		filename += std::to_string(texSize[0]);
		filename += "x";
		filename += std::to_string(texSize[0]);
		filename += ", ";

		precision_stream.str("");
		precision_string = precision_stream.str();

		filename += "scale ";
		precision_stream << std::fixed << std::setprecision(0) << textureScale;
		precision_string = precision_stream.str();
		filename += precision_string;

		filename += ", thli ";
		filename += std::to_string(thetalight2);

		filename += ", phli ";
		filename += std::to_string(philight2);

		filename += ", thvi ";
		filename += std::to_string(thetaview2);

		filename += ", phvi ";
		filename += std::to_string(phiview2);
	}

	writeCurrentFrameToBMP(std::string(filepath) + std::string(filename) + std::string(".bmp"), 0, 0, windowWidth, windowHeight);
	std::cout << "written current frame to " << filepath << filename << ".bmp" << std::endl;

	//foveated = true;

	//display();	display(); // to ensure that the display matches the bmp

	//writeCurrentFrameToBMP(std::string(filename + std::to_string(screenshotCounter) + std::string(" fov.bmp")), 0, 0, windowWidth, windowHeight);
	//std::cout << "written current frame to " << filename << screenshotCounter << ".bmp" << std::endl;
	++screenshotCounter;
	++screenshotCounter;

	//foveated = current_foveated;

	TwDefine(" gui visible=true ");
	showCoordSys_ = current_showCoordSys_;

	glutPostRedisplay(); // to ensure that the display matches the bmp
	display();  display();
}

void TW_CALL saveImage(void*)
{
	key('a', 0, 0);
	saveImagePNG();
}

void TW_CALL reloadTex1(void*)
{
	if (texID[0].size() > 0)
	{
		glDeleteTextures(texID[0].size(), &texID[0][0]);
	}

	bool validated = true;

	validated = initTexture(0);

	if (validated == false)
	{
		std::cerr << "error: unable to load new texture data" << std::endl;
	}
}

// Martin Johr //
void TW_CALL reloadTex1_2(void*)
{
	if (texID[1].size() > 0)
	{
		glDeleteTextures(texID[1].size(), &texID[1][0]);
	}

	bool validated = true;

	validated = initTexture(1);

	if (validated == false)
	{
		std::cerr << "error: unable to load new texture data" << std::endl;
	}
}


// Martin Johr //
/*void TW_CALL reloadTex2(void*)
{
if(texID[1].size() > 0)
{
glDeleteTextures(texID[1].size(), &texID[1][0]);
}

bool validated = true;

validated = initTexture(1);

if (validated == false)
{
std::cerr << "error: unable to load new texture data" << std::endl;
}
}*/

void TW_CALL reloadObj(void*)
{
	bool validated = true;

	validated = initObj();

	if (validated == false)
	{
		std::cerr << "error: unable to load new obj" << std::endl;
	}
}

void TW_CALL setRotation(const void* newVal, void *)
{
	rotation = *(const glm::vec4*) newVal;

	setMatrixUniforms();
}
void TW_CALL getRotation(void* val, void *) { *(glm::vec4*) val = rotation; }

void TW_CALL setZoom(const void* newVal, void *)
{
	zoom = *(const float*)newVal;

	setMatrixUniforms();
}
void TW_CALL getZoom(void* val, void *) { *(float*)val = zoom; }

void TW_CALL setShiftx(const void* newVal, void *)
{
	shiftx = *(const float*)newVal;

	setMatrixUniforms();
}
void TW_CALL getShiftx(void* val, void *) { *(float*)val = shiftx; }

void TW_CALL setShifty(const void* newVal, void *)
{
	shifty = *(const float*)newVal;

	setMatrixUniforms();
}
void TW_CALL getShifty(void* val, void *) { *(float*)val = shifty; }

void TW_CALL setLightPosX(const void* newVal, void *)
{
	lightPosition.x = *(const float*)newVal;

	setMatrixUniforms();
}
void TW_CALL getLightPosX(void* val, void *) { *(float*)val = lightPosition.x; }
void TW_CALL setLightPosY(const void* newVal, void *)
{
	lightPosition.y = *(const float*)newVal;

	setMatrixUniforms();
}
void TW_CALL getLightPosY(void* val, void *) { *(float*)val = lightPosition.y; }
void TW_CALL setLightPosZ(const void* newVal, void *)
{
	lightPosition.z = *(const float*)newVal;

	setMatrixUniforms();
}
void TW_CALL getLightPosZ(void* val, void *) { *(float*)val = lightPosition.z; }


void TW_CALL setThetaLight1(const void* newVal, void *)
{
	thetalight1 = *(const TextureFrequ*)newVal;

	GLuint loc = glGetUniformLocation(btfProgramID, "thetaLight1");
	glProgramUniform1f(btfProgramID, loc, float(thetalight1));
}
void TW_CALL getThetaLight1(void* val, void *) { *(TextureFrequ*)val = thetalight1; }

void TW_CALL setPhiLight1(const void* newVal, void *)
{
	philight1 = *(const TextureFrequ*)newVal;

	GLuint loc = glGetUniformLocation(btfProgramID, "phiLight1");
	glProgramUniform1f(btfProgramID, loc, float(philight1));
}
void TW_CALL getPhiLight1(void* val, void *) { *(TextureFrequ*)val = philight1; }

void setThetaLight2x(TextureFrequ newVal)
{
	thetalight2 = newVal;

	GLuint loc = glGetUniformLocation(btfProgramID, "thetaLight2");
	glProgramUniform1f(btfProgramID, loc, float(thetalight2));
}
void TW_CALL setThetaLight2(const void* newVal, void *)
{
	thetalight2 = *(const TextureFrequ*)newVal;

	GLuint loc = glGetUniformLocation(btfProgramID, "thetaLight2");
	glProgramUniform1f(btfProgramID, loc, float(thetalight2));
}
void TW_CALL getThetaLight2(void* val, void *) { *(TextureFrequ*)val = thetalight2; }

void setPhiLight2x(TextureFrequ newVal)
{
	philight2 = newVal;

	GLuint loc = glGetUniformLocation(btfProgramID, "phiLight2");
	glProgramUniform1f(btfProgramID, loc, float(philight2));
}
void TW_CALL setPhiLight2(const void* newVal, void *)
{
	philight2 = *(const TextureFrequ*)newVal;

	GLuint loc = glGetUniformLocation(btfProgramID, "phiLight2");
	glProgramUniform1f(btfProgramID, loc, float(philight2));
}
void TW_CALL getPhiLight2(void* val, void *) { *(TextureFrequ*)val = philight2; }

void TW_CALL setThetaView1(const void* newVal, void *)
{
	thetaview1 = *(const TextureFrequ*)newVal;

	GLuint loc = glGetUniformLocation(btfProgramID, "thetaView1");
	glProgramUniform1f(btfProgramID, loc, float(thetaview1));
}
void TW_CALL getThetaView1(void* val, void *) { *(TextureFrequ*)val = thetaview1; }

void TW_CALL setPhiView1(const void* newVal, void *)
{
	phiview1 = *(const TextureFrequ*)newVal;

	GLuint loc = glGetUniformLocation(btfProgramID, "phiView1");
	glProgramUniform1f(btfProgramID, loc, float(phiview1));
}
void TW_CALL getPhiView1(void* val, void *) { *(TextureFrequ*)val = phiview1; }

void setThetaView2x(TextureFrequ newVal)
{
	thetaview2 = newVal;

	GLuint loc = glGetUniformLocation(btfProgramID, "thetaView2");
	glProgramUniform1f(btfProgramID, loc, float(thetaview2));
}
void TW_CALL setThetaView2(const void* newVal, void *)
{
	thetaview2 = *(const TextureFrequ*)newVal;

	GLuint loc = glGetUniformLocation(btfProgramID, "thetaView2");
	glProgramUniform1f(btfProgramID, loc, float(thetaview2));
}
void TW_CALL getThetaView2(void* val, void *) { *(TextureFrequ*)val = thetaview2; }

void setPhiView2x(TextureFrequ newVal)
{
	phiview2 = newVal;

	GLuint loc = glGetUniformLocation(btfProgramID, "phiView2");
	glProgramUniform1f(btfProgramID, loc, float(phiview2));
}
void TW_CALL setPhiView2(const void* newVal, void *)
{
	phiview2 = *(const TextureFrequ*)newVal;

	GLuint loc = glGetUniformLocation(btfProgramID, "phiView2");
	glProgramUniform1f(btfProgramID, loc, float(phiview2));
}
void TW_CALL getPhiView2(void* val, void *) { *(TextureFrequ*)val = phiview2; }

void setTexScale1x(int newVal)
{
	textureScale = newVal;

	GLuint loc1 = glGetUniformLocation(btfProgramID, "tcm1"); // in fragment shader only tcm1 get used
	glProgramUniform1f(btfProgramID, loc1, float(textureScale));
}
void TW_CALL setTexScale1(const void* newVal, void *)
{
	textureScale = *(const float*)newVal;

	GLuint loc1 = glGetUniformLocation(btfProgramID, "tcm1"); // in fragment shader only tcm1 get used
	glProgramUniform1f(btfProgramID, loc1, float(textureScale));
}
void TW_CALL getTexScale1(void* val, void *) { *(float*)val = textureScale; }

void setTexScale2x(int newVal)
{
	textureScale = newVal;

	GLuint loc = glGetUniformLocation(btfProgramID, "tcm2");
	glProgramUniform1f(btfProgramID, loc, float(textureScale));
}
void TW_CALL setTexScale2(const void* newVal, void *)
{
	textureScale = *(const float*)newVal;

	GLuint loc = glGetUniformLocation(btfProgramID, "tcm2");
	glProgramUniform1f(btfProgramID, loc, float(textureScale));
}
void TW_CALL getTexScale2(void* val, void *) { *(float*)val = textureScale; }

void TW_CALL setEnableMipmaps1(const void* newVal, void *)
{
	mipmappinglo = *(const bool*)newVal;

	if (mipmappinglo == true)
	{
		glSamplerParameteri(samplers[0], GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	}
	else
	{
		glSamplerParameteri(samplers[0], GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	}
}
void TW_CALL getEnableMipmaps1(void* val, void *) { *(bool*)val = mipmappinglo; }

void TW_CALL setEnableMipmaps2(const void* newVal, void *)
{
	mipmappingro = *(const bool*)newVal;

	if (mipmappingro == true)
	{
		glSamplerParameteri(samplers[1], GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	}
	else
	{
		glSamplerParameteri(samplers[1], GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	}
}
void TW_CALL getEnableMipmaps2(void* val, void *) { *(bool*)val = mipmappingro; }

void TW_CALL setUserScreenDistance(const void* newVal, void *)
{
	UserScreenDistance_mm = *(const float*)newVal;
	calculateRadius_mm();
	calculate_CyclesPerDegree();
}
void TW_CALL getUserScreenDistance(void* val, void *) { *(float*)val = UserScreenDistance_mm; }


void TW_CALL setScreenWidth_mm(const void* newVal, void *)
{
	ScreenWidth_mm = *(const float*)newVal;
	calculateRadius_px();
	calculate_CyclesPerDegree();
}
void TW_CALL getScreenWidth_mm(void* val, void *) { *(float*)val = ScreenWidth_mm; }


void TW_CALL setScreenWidth_px(const void* newVal, void *)
{
	ScreenWidth_px = *(const float*)newVal;
	calculateRadius_px();
}
void TW_CALL getScreenWidth_px(void* val, void *) { *(float*)val = ScreenWidth_px; }

void setVisionSpanDiameter_degx(int newVal)
{
	VisionSpanDiameter_deg = newVal;
	calculateRadius_mm();
}
void TW_CALL setVisionSpanDiameter_deg(const void* newVal, void *)
{
	VisionSpanDiameter_deg = *(const float*)newVal;
	calculateRadius_mm();
}
void TW_CALL getVisionSpanDiameter_deg(void* val, void *) { *(float*)val = VisionSpanDiameter_deg; }


void TW_CALL set_mm_per_cycle(const void* newVal, void *)
{
	mm_per_cycle = *(const float*)newVal;
	calculate_CyclesPerDegree();
}
void TW_CALL get_mm_per_cycle(void* val, void *) { *(float*)val = mm_per_cycle; }

void setSmoothSwitchx(SmoothSwitch newVal)
{
	Smooth_Switch = newVal;

	GLuint swLoc = glGetUniformLocation(btfProgramID, "smoothswitch");
	glProgramUniform1i(btfProgramID, swLoc, Smooth_Switch);
}
void TW_CALL setSmoothSwitch(const void* newVal, void *)
{
	Smooth_Switch = *(const SmoothSwitch*)newVal;

	GLuint swLoc = glGetUniformLocation(btfProgramID, "smoothswitch");
	glProgramUniform1i(btfProgramID, swLoc, Smooth_Switch);
}
void TW_CALL getSmoothSwitch(void* val, void *) { *(SmoothSwitch*)val = Smooth_Switch; }


// Martin Johr //
/*void TW_CALL setObjCount(const void* newVal, void *)
{
bool validated = true;
if (objCount == *(const int*) newVal)
{
return;
}
objCount = *(const int*) newVal;
viewportWidth = (windowWidth-240)/objCount;

projectionMatrix = glm::perspective(90.0f, float(viewportWidth) / float(windowHeight), nearPlane, farPlane);
setMatrixUniforms();

if (objCount == 1)
{
validated = TwRemoveVar(gui, "emptyline4");
validated = TwRemoveVar(gui, "right object:");
validated = TwRemoveVar(gui, "texturero");
validated = TwRemoveVar(gui, "texturesizero");
validated = TwRemoveVar(gui, "texturetypero");
validated = TwRemoveVar(gui, "Loadtexturero");
validated = TwRemoveVar(gui, "emptyline5");
validated = TwRemoveVar(gui, "rotexscale");
validated = TwRemoveVar(gui, "romipmap");
validated = TwRemoveVar(gui, "rotle");
validated = TwRemoveVar(gui, "rople");
validated = TwRemoveVar(gui, "rotve");
validated = TwRemoveVar(gui, "ropve");
}
else //if(objCount == 2)
{
TwType tst = TwDefineEnum("Texturesize", tsEV, 4);
TwType tdt = TwDefineEnum("Texturedatatype", itEV, 3);
TwType tle = TwDefineEnum("ThetaLightEach", tEV, 3);
TwType ple = TwDefineEnum("PhiLightEach", tEV, 6);
TwType tve = TwDefineEnum("ThetaViewEach", tEV, 3);
TwType pve = TwDefineEnum("PhiViewEach", tEV, 6);

validated = TwAddButton(gui, "emptyline4", nullptr, nullptr, " label=' ' ");

validated = TwAddButton(gui, "right object:", nullptr, nullptr, nullptr);

validated = TwAddVarRW(gui, "texturero", TW_TYPE_CSSTRING(sizeof(texObject[1])), texObject[1], " label='texture' ");

validated = TwAddVarRW(gui, "texturesizero", tst, &texSize[1], " label='texture size' ");

validated = TwAddVarRW(gui, "texturetypero", tdt, &imgType[1], " label='texture type' ");

validated = TwAddButton(gui, "Loadtexturero", reloadTex2, nullptr, " label=' == Load Texture == ' ");

validated = TwAddButton(gui, "emptyline5", nullptr, nullptr, " label=' ' ");
validated = TwAddVarCB(gui, "rotexscale", TW_TYPE_FLOAT, setTexScale2, getTexScale2, nullptr, " min=0.0 label='scale texture' ");

validated = TwAddVarCB(gui, "romipmap", TW_TYPE_BOOLCPP, setEnableMipmaps2, getEnableMipmaps2, nullptr, " label='enable mipmaps' ");

validated = TwAddVarCB(gui, "rotle", tle, setThetaLight2, getThetaLight2, nullptr, " label='theta light, each' ");

validated = TwAddVarCB(gui, "rople", ple, setPhiLight2, getPhiLight2, nullptr,  " label='phi light, each' ");

validated = TwAddVarCB(gui, "rotve", tve, setThetaView2, getThetaView2, nullptr, " label='theta View, each' ");

validated = TwAddVarCB(gui, "ropve", pve, setPhiView2, getPhiView2, nullptr,  " label='phi View, each' ");
}

if (validated == false)
{
std::cerr << "error: unable to change number of objs" << std::endl;
}

}
void TW_CALL getObjCount(void* val, void *) {*(int*) val = objCount; }*/

bool initAntTweakBar()
{
	int validated = 1;

	TwInit(TW_OPENGL_CORE, NULL);
	TwWindowSize(windowWidth, windowHeight);
	gui = TwNewBar("gui");
	TwDefine(" gui position='0 0' size='350 900' resizable=false ");
	TwDefine(" TW_HELP visible=false ");

	TwType tst = TwDefineEnum("Texturesize", tsEV, 8);
	TwType tdt = TwDefineEnum("Texturedatatype", itEV, 3);
	TwType tle = TwDefineEnum("ThetaLightEach", tEV, 3);
	TwType ple = TwDefineEnum("PhiLightEach", tEV, 6);
	TwType tve = TwDefineEnum("ThetaViewEach", tEV, 3);
	TwType pve = TwDefineEnum("PhiViewEach", tEV, 6);
	TwType swe = TwDefineEnum("SmoothSwitch", swEV, 4);

	validated = TwAddButton(gui, "save image", saveImage, nullptr, " label=' == Save Image == ' ");

	validated = TwAddButton(gui, "emptyline0", nullptr, nullptr, " label=' ' ");

	validated = TwAddVarRW(gui, "obj", TW_TYPE_CSSTRING(sizeof(objFile)), objFile, nullptr);

	//validated = TwAddVarCB(gui, "obj count", TW_TYPE_INT32, setObjCount, getObjCount, nullptr, " min=1 max=2 ");

	validated = TwAddButton(gui, " == Load OBJ == ", reloadObj, nullptr, nullptr);

	validated = TwAddButton(gui, "emptyline1", nullptr, nullptr, " label=' ' ");


	validated = TwAddVarRW(gui, "background", TW_TYPE_COLOR4F, glm::value_ptr(backgroundColor), nullptr);

	validated = TwAddButton(gui, "emptyline1b", nullptr, nullptr, " label=' ' ");

	validated = TwAddVarCB(gui, "rotation", TW_TYPE_QUAT4F, setRotation, getRotation, nullptr, "showval=true");

	validated = TwAddVarCB(gui, "zoom", TW_TYPE_FLOAT, setZoom, getZoom, nullptr, " step=0.001 ");

	validated = TwAddVarCB(gui, "shift x", TW_TYPE_FLOAT, setShiftx, getShiftx, nullptr, " step=0.001 ");

	validated = TwAddVarCB(gui, "shift y", TW_TYPE_FLOAT, setShifty, getShifty, nullptr, " step=0.001 ");

	validated = TwAddButton(gui, "emptyline1c", nullptr, nullptr, " label=' ' ");

	validated = TwAddButton(gui, "light position", nullptr, nullptr, nullptr);

	validated = TwAddVarCB(gui, "light position x", TW_TYPE_FLOAT, setLightPosX, getLightPosX, nullptr, " label='x' step=0.10 ");
	validated = TwAddVarCB(gui, "light position y", TW_TYPE_FLOAT, setLightPosY, getLightPosY, nullptr, " label='y' step=0.10 ");
	validated = TwAddVarCB(gui, "light position z", TW_TYPE_FLOAT, setLightPosZ, getLightPosZ, nullptr, " label='z' step=0.10 ");

	validated = TwAddButton(gui, "emptyline1d", nullptr, nullptr, " label=' ' ");

	validated = TwAddVarRW(gui, "display coord-sys", TW_TYPE_BOOLCPP, &showCoordSys_, nullptr);
	validated = TwAddVarRW(gui, "display light bulb", TW_TYPE_BOOLCPP, &showLightBulb, nullptr);


	validated = TwAddButton(gui, "emptyline2", nullptr, nullptr, " label=' ' ");

	validated = TwAddButton(gui, "first texture:", nullptr, nullptr, nullptr);

	validated = TwAddVarRW(gui, "texture", TW_TYPE_CSSTRING(sizeof(texObject[0])), texObject[0], nullptr);

	validated = TwAddVarRW(gui, "texture size", tst, &texSize[0], nullptr);

	validated = TwAddVarRW(gui, "texture type", tdt, &imgType[0], nullptr);

	validated = TwAddButton(gui, " == Load Texture == ", reloadTex1, nullptr, nullptr);

	// Martin Johr ->
	validated = TwAddButton(gui, "emptyline2_2", nullptr, nullptr, " label=' ' ");

	validated = TwAddButton(gui, "second texture:", nullptr, nullptr, nullptr);

	validated = TwAddVarRW(gui, "texture 2", TW_TYPE_CSSTRING(sizeof(texObject[1])), texObject[1], nullptr);

	validated = TwAddVarRW(gui, "texture size 2", tst, &texSize[1], nullptr);

	validated = TwAddVarRW(gui, "texture type 2", tdt, &imgType[1], nullptr);

	validated = TwAddButton(gui, " == Load second Texture == ", reloadTex1_2, nullptr, nullptr);
	//<- Martin Johr //

	validated = TwAddButton(gui, "emptyline3", nullptr, nullptr, " label=' ' ");


	validated = TwAddVarCB(gui, "lotexscale", TW_TYPE_FLOAT, setTexScale1, getTexScale1, nullptr, " min=0.0 label='scale texture' ");

	validated = TwAddVarCB(gui, "lomipmap", TW_TYPE_BOOLCPP, setEnableMipmaps1, getEnableMipmaps1, nullptr, " label='enable mipmaps' ");

	validated = TwAddVarCB(gui, "lotle", tle, setThetaLight2, getThetaLight2, nullptr, " label='theta light, each' ");

	validated = TwAddVarCB(gui, "lople", ple, setPhiLight2, getPhiLight2, nullptr, " label='phi light, each' ");

	validated = TwAddVarCB(gui, "lotve", tve, setThetaView2, getThetaView2, nullptr, " label='theta View, each' ");

	validated = TwAddVarCB(gui, "lopve", pve, setPhiView2, getPhiView2, nullptr, " label='phi View, each' ");


	validated = TwAddButton(gui, "emptyline4", nullptr, nullptr, " label=' ' ");
	validated = TwAddButton(gui, "emptyline5", nullptr, nullptr, " label=' ' ");


	validated = TwAddVarCB(gui, "SmoothSwitch", swe, setSmoothSwitch, getSmoothSwitch, nullptr, " label='SmoothSwitch' ");

	validated = TwAddVarCB(gui, "UserScreenDistance", TW_TYPE_FLOAT, setUserScreenDistance, getUserScreenDistance, nullptr, " min=0.0 label='UserScreenDistance' ");

	validated = TwAddVarCB(gui, "ScreenWidth_mm", TW_TYPE_FLOAT, setScreenWidth_mm, getScreenWidth_mm, nullptr, " min=0.0 label='ScreenWidth_mm' ");

	validated = TwAddVarCB(gui, "ScreenWidth_px", TW_TYPE_FLOAT, setScreenWidth_px, getScreenWidth_px, nullptr, " min=0.0 label='ScreenWidth_px' ");

	validated = TwAddVarCB(gui, "VisionSpanDiameter_deg", TW_TYPE_FLOAT, setVisionSpanDiameter_deg, getVisionSpanDiameter_deg, nullptr, " min=0.0 label='VisionSpanDiameter_deg' ");

	validated = TwAddVarCB(gui, "mm per cycle", TW_TYPE_FLOAT, set_mm_per_cycle, get_mm_per_cycle, nullptr, " label='mm per cycle' ");

	validated = TwAddVarRO(gui, "window width", TW_TYPE_UINT16, &windowWidth, nullptr);

	TwAddSeparator(gui, "Sep1", "");

	calculateRadius_mm();
	calculateRadius_px();
	calculate_CyclesPerDegree();

	validated = TwAddVarRO(gui, "Radius_mm", TW_TYPE_FLOAT, &Radius_mm, nullptr);

	validated = TwAddVarRO(gui, "Radius_px", TW_TYPE_UINT16, &Radius_px, nullptr);

	validated = TwAddVarRO(gui, "CyclesPerDegree", TW_TYPE_FLOAT, &CyclesPerDegree, nullptr);

	validated = TwAddVarRO(gui, "MillimeterPerDegree", TW_TYPE_FLOAT, &MillimeterPerDegree, nullptr);

	validated = TwAddButton(gui, "emptyline6", nullptr, nullptr, " label=' ' ");

	validated = TwAddButton(gui, " == Create Test Pictures == ", createTestImages, nullptr, nullptr);

	validated = TwAddVarRW(gui, "in folder", TW_TYPE_CSSTRING(sizeof(foldername)), foldername, nullptr);

	return validated == 1;
}

//	|  Martin Johr //
void setTexUniforms(std::vector<GLuint> const& tex, unsigned int texNum)
{
	for (unsigned int i = 0; i < tex.size(); ++i)
	{
		glActiveTexture(GL_TEXTURE0 + i + texNum * textureOffset);
		glBindTexture(GL_TEXTURE_2D_ARRAY, tex[i]);
		// |					Martin Johr																				 | //
		std::string t = std::string("texArray[") + std::to_string(static_cast<long long>(i + texNum * textureOffset)) + std::string("]"); // ") + std::to_string(texNum) + std::string("[") + std::to_string(static_cast<long long>(i)) + std::string("]");
		GLuint tLoc = glGetUniformLocation(btfProgramID, t.c_str());
		glProgramUniform1i(btfProgramID, tLoc, i + texNum * textureOffset);
	}

	GLuint tsLoc = glGetUniformLocation(btfProgramID, "texArraySize");
	glProgramUniform1i(btfProgramID, tsLoc, tex.size());

	GLuint tcLoc = glGetUniformLocation(btfProgramID, "texCounter");
	glProgramUniform1i(btfProgramID, tcLoc, texCounter);
}


void setMatrixUniforms()
{
	glm::mat4 vm, mm, ml;
	if (switchLightMoveMode)
	{
		ml = glm::translate(glm::mat4(1.0f), lightPosition);
	}
	else
	{
		ml = modelMatrixLight;
	}

	if (switchRotZoomMode)
	{
		float yy2 = 2.0f * rotation[1] * rotation[1];
		float xy2 = 2.0f * rotation[0] * rotation[1];
		float xz2 = 2.0f * rotation[0] * rotation[2];
		float yz2 = 2.0f * rotation[1] * rotation[2];
		float zz2 = 2.0f * rotation[2] * rotation[2];
		float wz2 = 2.0f * rotation[3] * rotation[2];
		float wy2 = 2.0f * rotation[3] * rotation[1];
		float wx2 = 2.0f * rotation[3] * rotation[0];
		float xx2 = 2.0f * rotation[0] * rotation[0];
		mm[0][0] = -yy2 - zz2 + 1.0f;
		mm[0][1] = xy2 + wz2;
		mm[0][2] = xz2 - wy2;
		mm[0][3] = 0.0f;

		mm[1][0] = xy2 - wz2;
		mm[1][1] = -xx2 - zz2 + 1.0f;
		mm[1][2] = yz2 + wx2;
		mm[1][3] = 0.0f;

		mm[2][0] = xz2 + wy2;
		mm[2][1] = yz2 - wx2;
		mm[2][2] = -xx2 - yy2 + 1.0f;
		mm[2][3] = 0.0f;

		mm[3][0] = mm[3][1] = mm[3][2] = 0.0f;
		mm[3][3] = 1.0f;

		vm = glm::translate(glm::mat4(1.0f), glm::vec3(shiftx, shifty, zoom));
	}
	else
	{
		mm = modelMatrix;
		vm = viewMatrix;
	}

	//obj
	glm::mat4 mvMatrix = vm * mm;
	GLuint mvLoc = glGetUniformLocation(btfProgramID, "mv");
	glProgramUniformMatrix4fv(btfProgramID, mvLoc, 1, GL_FALSE, glm::value_ptr(mvMatrix));

	glm::mat4 mvpMatrix = projectionMatrix * vm * mm;
	GLuint mvpLoc = glGetUniformLocation(btfProgramID, "mvp");
	glProgramUniformMatrix4fv(btfProgramID, mvpLoc, 1, GL_FALSE, glm::value_ptr(mvpMatrix));

	//light
	glm::mat4 vplMatrix = vm * ml;
	GLuint vplLoc = glGetUniformLocation(lightProgramID, "vp");
	glProgramUniformMatrix4fv(lightProgramID, vplLoc, 1, GL_FALSE, glm::value_ptr(vplMatrix));

	glm::mat4 mvplMatrix = projectionMatrix * vm * ml;
	GLuint mvplLoc = glGetUniformLocation(lightProgramID, "mvp");
	glProgramUniformMatrix4fv(lightProgramID, mvplLoc, 1, GL_FALSE, glm::value_ptr(mvplMatrix));

	glm::vec3 pl = glm::vec3(vm * ml * glm::vec4(0.0f, 0.0f, 0.0f, 1.0f));
	GLuint lLoc = glGetUniformLocation(btfProgramID, "posLightEye");
	glProgramUniform3fv(btfProgramID, lLoc, 1, glm::value_ptr(pl));

	//coordsys
	glm::mat4 vpcMatrix = projectionMatrix * vm;
	GLuint vpcLoc = glGetUniformLocation(coordSysProgramID, "vp");
	glProgramUniformMatrix4fv(coordSysProgramID, vpcLoc, 1, GL_FALSE, glm::value_ptr(vpcMatrix));
}


void setUniforms()
{
	for (unsigned int i = 0; i < texID.size(); ++i)
	{						//	 | Martin Johr //
		setTexUniforms(texID[i], i);
	}

	setMatrixUniforms();


	GLuint tpiLoc = glGetUniformLocation(btfProgramID, "phiInterval");
	glProgramUniform1fv(btfProgramID, tpiLoc, sizeof(phiInterval) / sizeof(float), phiInterval);

	GLuint tpicLoc = glGetUniformLocation(btfProgramID, "phiIntervalCount");
	glProgramUniform1fv(btfProgramID, tpicLoc, sizeof(phiIntervalCount) / sizeof(float), phiIntervalCount);

	GLuint ttpvdLoc = glGetUniformLocation(btfProgramID, "texPerViewDir");
	glProgramUniform1ui(btfProgramID, ttpvdLoc, texPerViewDir);

	GLuint ttiLoc = glGetUniformLocation(btfProgramID, "thetaInterval");
	glProgramUniform1f(btfProgramID, ttiLoc, thetaInterval);
}


void display()
{
	glClearColor(backgroundColor[0], backgroundColor[1], backgroundColor[2], backgroundColor[3]);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	frames += 1;

	/*frametime = glutGet(GLUT_ELAPSED_TIME);
	if ((frametime - base_frametime) > 1000.0)
	{
	fps = frames*1000.0 / (frametime - base_frametime);
	base_frametime = frametime;
	frames = 0;
	}
	std::cout << "fps: " << fps << std::endl;*/

	if (switchPOI)
	{
		GLuint poiLoc = glGetUniformLocation(btfProgramID, "POI");
		glProgramUniform2fv(btfProgramID, poiLoc, 1, glm::value_ptr(mousi));
	}
	else
	{
		GLuint poiLoc = glGetUniformLocation(btfProgramID, "POI");
		glProgramUniform2fv(btfProgramID, poiLoc, 1, glm::value_ptr(center_));
	}

	GLuint rad1Loc = glGetUniformLocation(btfProgramID, "Radius_px");
	glProgramUniform1i(btfProgramID, rad1Loc, Radius_px);

	GLuint rad2Loc = glGetUniformLocation(btfProgramID, "radius_two");
	glProgramUniform1i(btfProgramID, rad2Loc, radius_two);

	GLuint fovLoc = glGetUniformLocation(btfProgramID, "foveated");
	glProgramUniform1i(btfProgramID, fovLoc, foveated);

	GLuint intswLoc = glGetUniformLocation(btfProgramID, "interval_switch");
	glProgramUniform1i(btfProgramID, intswLoc, interval_switch);

	for (unsigned int i = 0; i < texID.size(); ++i) //objCount; ++i)
	{
		setTexUniforms(texID[i], i);

		for (unsigned int j = 0; j < texID[i].size(); ++j)
		{
			glBindSampler(j, samplers[i]);
		}

		glViewport(i * viewportWidth, 0, viewportWidth, windowHeight);

		glUseProgram(btfProgramID);
		GLuint oloc1 = glGetUniformLocation(btfProgramID, "objectID");
		glUniform1i(oloc1, i);
		obj.draw();


		if (showLightBulb)
		{
			glBindVertexArray(vaoLightID);
			glUseProgram(lightProgramID);
			glDrawArrays(GL_POINTS, 0, 1);
		}

		if (showCoordSys_)
		{
			glBindVertexArray(vaoCoordSysID);
			glUseProgram(coordSysProgramID);
			glDrawArrays(GL_LINES, 0, 6);
		}

		for (unsigned int j = 0; j < texID[i].size(); ++j)
		{
			glBindSampler(j, 0);
		}
	}

	glViewport(0, 0, windowWidth, windowHeight);

	TwDraw();
	glutSwapBuffers();
}


bool cleanupGL()
{
	bool validated = true;

	glDeleteProgram(btfProgramID);
	glDeleteProgram(lightProgramID);
	glDeleteProgram(coordSysProgramID);

	for (unsigned int i = 0; i < texID.size(); ++i)
	{
		if (texID[i].size() > 0)
		{
			glDeleteTextures(texID[i].size(), &texID[i][0]);
		}
	}
	glDeleteSamplers(2, &samplers[0]);

	glDeleteVertexArrays(1, &vaoLightID);
	glDeleteBuffers(1, &vboLightID);
	glDeleteVertexArrays(1, &vaoCoordSysID);
	glDeleteBuffers(1, &vboCoordSysID);

	return validated;
}


void calculateRadius_mm()
{
	float tan_x = tan(VisionSpanDiameter_deg / 2 * PI / 180.0);
	Radius_mm = tan_x * UserScreenDistance_mm;
	calculateRadius_px();
}


void calculateRadius_px()
{
	Radius_px = ScreenWidth_px * Radius_mm / ScreenWidth_mm;
}


void calculate_MillimeterPerDegree()
{
	MillimeterPerDegree = ((UserScreenDistance_mm * PI) / 180);
}


void calculate_CyclesPerDegree()
{
	calculate_MillimeterPerDegree();
	CyclesPerDegree = MillimeterPerDegree * (1 / mm_per_cycle);
}


void calculateCenter()
{
	center_ = glm::vec3(windowWidth / 2, windowHeight / 2, 1.0);
}


void key(unsigned char key, int x, int y)
{
	GLuint tcLoc;

	TwEventKeyboardGLUT(key, x, y);
	switch (key)
	{
		/// press ESC or q to quit
	case 27:
	case 'Q':
		std::cout << "Q" << std::endl;
		cleanupGL();
		exit(0);
		break;
	case 'T':
		std::cout << "T" << std::endl;
		tcLoc = glGetUniformLocation(btfProgramID, "texCounter");
		texCounter += 10;

		glProgramUniform1i(btfProgramID, tcLoc, texCounter);
		std::cout << texCounter << std::endl;
		break;
	case 'S':
		std::cout << "S" << std::endl;
		initProgram(btfProgramID, btfVertShaderSource, btfFragShaderSource);
		initProgram(lightProgramID, lightVertShaderSource, lightFragShaderSource);
		initProgram(coordSysProgramID, coordSysVertShaderSource, coordSysFragShaderSource);
		setUniforms();

		break;
	case 'L':
		std::cout << "L" << std::endl;
		switchLightMoveMode = !switchLightMoveMode;
		setMatrixUniforms();
		break;
	case 'R':
		std::cout << "R" << std::endl;
		switchRotZoomMode = !switchRotZoomMode;
		setMatrixUniforms();
		break;
	case 'M':
		std::cout << "M" << std::endl;
		switchMouseFunc = !switchMouseFunc;
		break;
	case 'C':
		std::cout << "C" << std::endl;
		switchPOI = !switchPOI;
		break;
	case 'P':
		std::cout << "P" << std::endl;
		saveImage(nullptr);
		break;
	case 'F':
		std::cout << "F" << std::endl;
		foveated = !foveated;
		break;
	default:
		break;
	}

	glutPostRedisplay();
}

void specialKey(int key, int x, int y)
{

	switch (key)
	{
	case GLUT_KEY_LEFT:
	{
		viewMatrix = glm::translate(glm::mat4(1.0f), glm::vec3(1.0f, 0.0f, 0.0f)) * viewMatrix;
		break;
	}
	case GLUT_KEY_RIGHT:
	{
		viewMatrix = glm::translate(glm::mat4(1.0f), glm::vec3(-1.0f, 0.0f, 0.0f)) * viewMatrix;
		break;
	}
	case GLUT_KEY_UP:
	{
		viewMatrix = glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 1.0f, 0.0f)) * viewMatrix;
		break;
	}
	case GLUT_KEY_DOWN:
	{
		viewMatrix = glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, -1.0f, 0.0f)) * viewMatrix;
		break;
	}
	default:
	{
		break;
	}
	}
	setMatrixUniforms();

	glutPostRedisplay();
}


void mousimove(int x, int y)
{
	int invert_y = windowHeight - y;
	mousi.x = x;
	mousi.y = invert_y;

	glutPostRedisplay();
}

void motionFunc(int x, int y)
{
	if (!TwEventMouseMotionGLUT(x, y))
	{
		if (lastButton == GLUT_LEFT_BUTTON)
		{
			glm::vec3 v(0.0f);
			float d;
			v.x = (2.0f * x - windowWidth) / float(windowWidth);
			v.y = (windowHeight - 2.0f * y) / float(windowHeight);
			// v.z = 0.0f;
			d = glm::length(v);
			d = d < 1.0f ? d : 1.0f;
			v.z = std::sqrt(1.001f - d*d);
			v = glm::normalize(v);

			glm::vec3 curPos = v;

			glm::vec3 dir = curPos - lastMousePos;
			float velo = glm::length(dir);
			if (velo > 0.0001f)
			{
				glm::vec3 rotAxis = glm::cross(lastMousePos, curPos);
				float rotAngle = velo * 50.0f;

				if (switchMouseFunc)
				{
					modelMatrix = glm::rotate(glm::mat4(1.0f), rotAngle, rotAxis) * modelMatrix;
				}
				else
				{
					viewMatrix = glm::rotate(glm::mat4(1.0f), rotAngle, rotAxis) * viewMatrix;
				}
			}
			lastMousePos = curPos;
		}
		else if (lastButton == GLUT_RIGHT_BUTTON)
		{
			glm::vec3 curPos(x, y, 0.0f);
			float yDiff = curPos.y - lastMousePos.y;
			viewMatrix = glm::translate(glm::mat4(1.0f), glm::vec3(0.0, 0.0, 0.1f*yDiff)) * viewMatrix;

			lastMousePos = curPos;
		}
		else if (lastButton == GLUT_MIDDLE_BUTTON)
		{
			glm::vec3 v(0.0f);
			float d;
			v.x = (2.0f * x - windowWidth) / float(windowWidth);
			v.y = (windowHeight - 2.0f * y) / float(windowHeight);
			// v.z = 0.0f;
			d = glm::length(v);
			d = d < 1.0f ? d : 1.0f;
			v.z = std::sqrt(1.001f - d*d);
			v = glm::normalize(v);

			glm::vec3 curPos = v;

			glm::vec3 dir = curPos - lastMousePos;
			float velo = glm::length(dir);
			if (velo > 0.0001f)
			{
				float rotAngle = velo * 50.0f;

				modelMatrixLight = glm::rotate(glm::mat4(1.0f), rotAngle, glm::vec3(0.0f, 1.0f, 0.0f)) * modelMatrixLight;
			}
			lastMousePos = curPos;
		}
		setMatrixUniforms();

	}

	glutPostRedisplay();
}


void mouseFunc(int button, int state, int x, int y)
{
	if (!TwEventMouseButtonGLUT(button, state, x, y))
	{	// GLUT_WHEEL_UP - GLUT_WHEEL_DOWN
		if (button == GLUT_LEFT_BUTTON)
		{
			glm::vec3 v(0.0f);
			float d;
			v.x = (2.0f * x - windowWidth) / float(windowWidth);
			v.y = (windowHeight - 2.0f * y) / float(windowHeight);
			// v.z = 0.0f;
			d = glm::length(v);
			d = d < 1.0f ? d : 1.0f;
			v.z = std::sqrt(1.001f - d*d);
			v = glm::normalize(v);

			lastMousePos = v;
			lastButton = GLUT_LEFT_BUTTON;
		}
		else if (button == GLUT_RIGHT_BUTTON)
		{
			lastMousePos = glm::vec3(x, y, 0.0f);
			lastButton = GLUT_RIGHT_BUTTON;
		}
		else if (button == GLUT_MIDDLE_BUTTON)
		{
			glm::vec3 v(0.0f);
			float d;
			v.x = (2.0f * x - windowWidth) / float(windowWidth);
			v.y = (windowHeight - 2.0f * y) / float(windowHeight);
			// v.z = 0.0f;
			d = glm::length(v);
			d = d < 1.0f ? d : 1.0f;
			v.z = std::sqrt(1.001f - d*d);
			v = glm::normalize(v);

			lastMousePos = v;
			lastButton = GLUT_MIDDLE_BUTTON;
		}
		else if (button == 3)
		{
			if (VisionSpanDiameter_deg <= 60)
			{
				VisionSpanDiameter_deg += 0.1;
				calculateRadius_mm();
			}
		}
		else if (button == 4)
		{
			if (VisionSpanDiameter_deg >= 1)
			{
				VisionSpanDiameter_deg -= 0.1;
				calculateRadius_mm();
			}
		}
		else
		{
			lastButton = button;
		}
	}
	glutPostRedisplay();
}


void resize(int width, int height)
{
	windowWidth = width;
	windowHeight = height;
	viewportWidth = (windowWidth);// / objCount;

	glViewport(0, 0, windowWidth, windowHeight);
	projectionMatrix = glm::perspective(90.0f, float(viewportWidth) / float(windowHeight), nearPlane, farPlane);

	setMatrixUniforms();

	TwWindowSize(windowWidth, windowHeight);

	// Martin Johr
	calculateCenter();

	glutPostRedisplay();
}



bool initGL()
{
	bool validated = true;

	if (ogl_LoadFunctions() == ogl_LOAD_FAILED)
	{
		validated = false;
	}

	if (validated)
	{
		validated = (ogl_IsVersionGEQ(4, 2) == 1);
	}
	if (validated)
	{
		validated = initDebugOutput();
	}
	if (validated)
	{
		validated = initProgram(btfProgramID, btfVertShaderSource, btfFragShaderSource);
		validated = validated && initProgram(lightProgramID, lightVertShaderSource, lightFragShaderSource);
		validated = validated && initProgram(coordSysProgramID, coordSysVertShaderSource, coordSysFragShaderSource);
	}
	if (validated)
	{
		validated = initLight();
	}
	if (validated)
	{
		validated = initCoordSys();
	}
	if (validated)
	{
		//		validated = initTexture();
	}
	if (validated)
	{
		validated = initSamplers();
	}
	if (validated)
	{
		validated = initMatrices();
	}
	if (validated)
	{
		validated = initObj();
	}
	if (validated)
	{
		validated = initAntTweakBar();
	}

	if (validated)
	{
		glEnable(GL_DEPTH_TEST);
		glEnable(GL_PROGRAM_POINT_SIZE);
		glLineWidth(5.0f);
	}

	return validated;
}


bool WGLExtensionSupported(const char *extension_name)
{
	// this is pointer to function which returns pointer to string with list of all wgl extensions
	PFNWGLGETEXTENSIONSSTRINGEXTPROC _wglGetExtensionsStringEXT = NULL;

	// determine pointer to wglGetExtensionsStringEXT function
	_wglGetExtensionsStringEXT = (PFNWGLGETEXTENSIONSSTRINGEXTPROC)wglGetProcAddress("wglGetExtensionsStringEXT");

	if (strstr(_wglGetExtensionsStringEXT(), extension_name) == NULL)
	{
		// string was not found
		return false;
	}

	// extension is supported
	return true;
}


bool initGLUT(int argc, char* argv[])
{
	bool validated = true;


	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_RGBA | GLUT_ALPHA | GLUT_DOUBLE | GLUT_DEPTH);

	glutInitWindowPosition(10, 10);
	glutInitWindowSize(windowWidth, windowHeight);
	glutInitContextVersion(4, 2);
	glutInitContextProfile(GLUT_CORE_PROFILE);
	glutInitContextFlags(GLUT_DEBUG);
	glutCreateWindow(argv[0]);

	glutReshapeFunc(resize);

	glutDisplayFunc(display);

	glutFullScreen();

	glutKeyboardFunc(key);
	glutSpecialFunc(specialKey);

	glutIdleFunc(nullptr);

	glutPassiveMotionFunc(mousimove);
	glutMotionFunc(motionFunc);
	glutMouseFunc(mouseFunc);

	PFNWGLSWAPINTERVALEXTPROC       wglSwapIntervalEXT = NULL;
	PFNWGLGETSWAPINTERVALEXTPROC    wglGetSwapIntervalEXT = NULL;


	if (WGLExtensionSupported("WGL_EXT_swap_control"))
	{
		// Extension is supported, init pointers.
		wglSwapIntervalEXT = (PFNWGLSWAPINTERVALEXTPROC)wglGetProcAddress("wglSwapIntervalEXT");

		// this is another function from WGL_EXT_swap_control extension
		wglGetSwapIntervalEXT = (PFNWGLGETSWAPINTERVALEXTPROC)wglGetProcAddress("wglGetSwapIntervalEXT");

		wglSwapIntervalEXT(0);
	}


	return validated;
}


int main(int argc, char* argv[])
{
	bool result = true;

	initGLUT(argc, argv);

	result = initGL();
	if (result)
	{
		glutMainLoop();
	}
	else
	{
		std::cerr << "Unable to initialize OpenGL, exiting..." << std::endl;
	}

	cleanupGL();

	return result;
}



